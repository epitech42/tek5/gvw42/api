# API GVW

[![Coverage](https://sonarqube.grosseteub.fr/api/project_badges/measure?project=gvw%3Aapi&metric=coverage)](https://sonarqube.grosseteub.fr/dashboard?id=gvw%3Aapi)

[![Maintainability Rating](https://sonarqube.grosseteub.fr/api/project_badges/measure?project=gvw%3Aapi&metric=sqale_rating)](https://sonarqube.grosseteub.fr/dashboard?id=gvw%3Aapi)

[![Security Rating](https://sonarqube.grosseteub.fr/api/project_badges/measure?project=gvw%3Aapi&metric=security_rating)](https://sonarqube.grosseteub.fr/dashboard?id=gvw%3Aapi)

[![Duplicated Lines (%)](https://sonarqube.grosseteub.fr/api/project_badges/measure?project=gvw%3Aapi&metric=duplicated_lines_density)](https://sonarqube.grosseteub.fr/dashboard?id=gvw%3Aapi)

Requirements:
   - docker
   - docker-compose
   - make

Don't forget to init, if not, will not works (```make init```)

When http requests triggers a route, it triggers it in `index.js` and then the controllers call the appropriate `actions/` like `User.create()`.

## HTTP Status:
- Good, server return `200`.
- Wrong, server return `422`.

## Watcher:
- We are using `PM2` to watch files in local dev and relaunch node while changes.
- The config file is in `config/` directory.

## Timer:
- To use timer, import `actions/timer.js` and then use `Timer.display(start)` that return `[time in ms]`.
- `start` must be a `new Date()` or `Date.now()` to use start time.


