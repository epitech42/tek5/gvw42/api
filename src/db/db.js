const mongoose = require("mongoose");

const mongo = process.env.MONGO_URL || "mongo";
const mongoDbName = process.env.MONGO_DB_NAME || "gvw";

mongoose.connect(`mongodb://${mongo}/${mongoDbName}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.connection.on(
  "error",
  console.error.bind(console, "connection error:")
);
mongoose.connection.once("open", function() {
  console.log(`[DB] [MONGO] ${mongo}/${mongoDbName} connected`);
});

module.exports = mongoose;
