// SOCKET IO
const io = require("socket.io")();
const redisAdapter = require("socket.io-redis");

io.adapter(redisAdapter({ host: "redis", port: 6379 }));

//var udid = require('udid');
const { v4: uuidv4 } = require('uuid');

// Import Actions
const User = require("../../actions/user.js");
const Room = require("../../actions/room.js");
const Socket = require("../../actions/socket.js");
const Playlist = require("../../actions/video.js");

// Import Misc Actions
const Timer = require("../../actions/timer.js");
const log = require("../../actions/notifications.js");

const cache = require("../../db/cache.js").client;
const publisher = require("../../db/cache.js").publisher;
const subscriber = require("../../db/cache.js").subscriber;

subscriber.on("message",(channel,message) => {
    console.log("Received data :"+message);
})

const roomLoginEvent = "room:login";
const roomDisconnectEvent = "disconnect";

const roomLoadEvent = "room:load";

const usersConnectedEvent = "users:count";
const usersDisconnectedEvent = "users:disconnect";
const usersGetInfosEvent = "users:getInfos";

const messageEvent = "message";

const roomToggleUserPermEvent = "room:toggleUserPerm";

const success = ":success";
const err = ":err";

const roomAskAuthEvent = "room:askauth";

let start = new Date();

module.exports = function(client) {
  client.on(roomLoginEvent, token => {
    start = Date.now();
      User.verifyToken(token).then(value => {
	  if (value === true) {
              User.getUserfromToken(token).then(function(user) {
		  Socket.connect(user.userID, client.id).then(function(value) {
		      client.emit(roomLoginEvent + success, {"userID": user.userID, "message": value});
		      log.write(
			  `${Timer.display(start)}[${client.id}]` +
			      ` ${user.username} ${roomLoginEvent} ${value}`
		      );
		      Socket.addUserConnect(user.userID, client.id.substring(6, client.id.search("#"))).then(function(value){
			  client.emit(usersConnectedEvent, value);
			  client.broadcast.emit(usersConnectedEvent, value);
		      });
		      //subscriber.subscribe("users:connected:" + client.id.substring(0, client.id.search("#")));
		      //publisher.publish("users:connected:" + client.id.substring(0, client.id.search("#")), 1);
		      //client.join(user.roomID);
		  });
              });
	  } else {
              client.emit(roomLoginEvent + err, "NOT_CONNECTED");
              log.write(
		  `${Timer.display(start)}[${client.id}]` +
		      ` ${roomLoginEvent} ` +
		      `NOT_CONNECTED`
              );
	  }
      });
  });

    client.on(roomLoadEvent, function() {
	start = Date.now();
	Socket.isSocketIDConnected(client.id).then(value => {
	    console.log(value);
	    if (value === true) {
		const roomID = client.id.substring(6, client.id.search("#"));
		Socket.getUserFromClientID(client.id).then(function(user) {
		    Room.getRoomFromID(roomID)
			.then(function(value) {
			    client.emit(roomLoadEvent + success, value);
			})
			.catch(function(err) {
			    console.log(err);
			    client.emit(roomLoadEvent + err, err);
			});
		});
	    } else {
		client.emit(roomLoadEvent + err, "USER_NOT_CONNECTED");
		client.emit(roomAskAuthEvent, "USER_NOT_CONNECTED");
	    }
	});
    });

    client.on(messageEvent, function(message, time) {
	start = Date.now();
	Socket.isSocketIDConnected(client.id).then(value => {
            if (value === true) {
		const roomID = client.id.substring(6, client.id.search("#"));
		Socket.getUserFromClientID(client.id).then(function(user) {
                    log.write(
			`${Timer.display(start)}[${client.id}]` +
                            ` ${user.username} ${messageEvent} ${message}`
                    );
		    const data = {
			id: uuidv4(roomID),
			date: new Date(),
			userID: user.id,
			username: user.username,
			message,
			time,
		    }
                    client.emit(messageEvent + success, data);
                    client.broadcast.emit(messageEvent, data);
		});
            } else {
		client.emit(messageEvent + err, "USER_NOT_CONNECTED");
		client.emit(roomAskAuthEvent, "USER_NOT_CONNECTED");
            }
	});
    });
    
  client.on(usersGetInfosEvent, function(userID) {
      start = Date.now();
      Socket.isSocketIDConnected(client.id).then(async (value) => {
	  var users = [];
	  if (value === true) {
              const roomID = client.id.substring(6, client.id.search("#"));
	      for (i = 0; i < userID.length; i++){
		  User.getUserFromID(userID[i]).then(function(user) {
		      users.push({id: user._id, username: user.username});
		      if (user._id === userID[userID.length-1])
		      {
			  client.emit(usersGetInfosEvent + success, users);
			  log.write(`${Timer.display(start)}[${client.id}]` + ` ${usersGetInfosEvent} ${JSON.stringify(users)}`);
		      }
		  }).catch(function(err){
		      console.log(userID[i] + " " + err);
		  });
		  
		  //users.push(User.getUserFromID(userID[i]));
	      }
	      //setTimeout(function(){
		  /*client.emit(usersGetInfosEvent + success, users);
		  log.write(
		      `${Timer.display(start)}[${client.id}]` +
			  ` ${usersGetInfosEvent} ${JSON.stringify(users)}`
		  );*/
	      //}, 20);
	  }
      });
});
    
    client.on(roomToggleUserPermEvent, function(userID) {
        start = Date.now();
        Socket.isSocketIDConnected(client.id).then(value => {
            if (value === true) {
                const roomID = client.id.substring(6, client.id.search("#"));
                Socket.getUserFromClientID(client.id).then(function(user) {
                    Room.toggleUserPerm(roomID, userID)
                        .then(function(value) {
			    log.write(`${Timer.display(start)}[${client.id}]` + ` ${roomToggleUserPermEvent} ${user.username} ${value}`);
                            client.emit(roomToggleUserPermEvent + success, value);
			    client.broadcast.emit(roomToggleUserPermEvent, value);
                        })
                        .catch(function(err) {
                            console.log(err);
                            client.emit(roomLoadEvent + err, err);
			});
                });
            } else {
	        client.emit(roomToggleUserPerm + err, "USER_NOT_CONNECTED");
                client.emit(roomAskAuthEvent, "USER_NOT_CONNECTED");
            }
        });
    });

    
  client.on(roomDisconnectEvent, function() {
    start = Date.now();
    Socket.isSocketIDConnected(client.id).then(function(value) {
	if (value === true) {
	    Socket.getUserFromClientID(client.id).then(function(user) {
		Socket.disconnect(client.id).then(function(value) {
		    log.write(
			`${Timer.display(start)}[${client.id}] ` + user.username + 
			    ` ${roomDisconnectEvent} ${value}`
		    );
		    Socket.deleteUserConnect(user.id, client.id.substring(6, client.id.search("#"))).then(function(value){
			client.broadcast.emit(usersDisconnectedEvent, user.id);
		    });
		});
	    });
      } else {
        log.write(
          `${Timer.display(start)}[${client.id}] ${roomDisconnectEvent}`
        );
      }
    });
  });
};
