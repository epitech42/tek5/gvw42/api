// SOCKET IO
const io = require("socket.io")();
const redisAdapter = require("socket.io-redis");

io.adapter(redisAdapter({ host: "redis", port: 6379 }));

// Import Actions
const User = require("../../actions/user.js");
const Room = require("../../actions/room.js");
const Socket = require("../../actions/socket.js");
const Playlist = require("../../actions/video.js");

// Import Misc Actions
const Timer = require("../../actions/timer.js");
const log = require("../../actions/notifications.js");

const cache = require("../../db/cache.js").client;

const videoAddEvent = "video:add";
const videoEditEvent = "video:edit";
const videoDeleteEvent = "video:delete";

const videoAutoPlayEvent = "video:autoplay";
const videoPlayStatusEvent = "video:playStatus";
const videoPlayerTimeEvent = "video:playerTime";
const videoOnSeekEvent = "video:seek";

const videoMoveUpEvent = "video:moveUp";
const videoMoveDownEvent = "video:moveDown";

const roomSetPasswordEvent = "room:setPassword";
const roomSetAdminPasswordEvent = "room:setAdminPassword";
const roomSetIsPasswordProtectedEvent = "room:setIsPasswordProtected";

const roomAskAuthEvent = "room:askauth";

const success = ":success";
const err = ":err";

let start = new Date();

module.exports = function(client) {
  client.on(videoAddEvent, function(url) {
      start = Date.now();
      Socket.isSocketIDConnected(client.id).then(value => {
	if (value === true) {
            const roomID = client.id.substring(6, client.id.search("#"));
            Socket.getUserFromClientID(client.id).then(function(user) {
		Playlist.AddVideo(roomID, user._id, url).then(value => {
		    log.write(
			`${Timer.display(start)}[${client.id}]` +
			    ` ${user.username} ${videoAddEvent} ${value}`
		    );
		    client.emit(videoAddEvent + success, value);
		    client.broadcast.emit(videoAddEvent, value);
		})
		    .catch(function(err) {
			console.log(err);
			client.emit(videoAddEvent + err, err);
		    });
            });
	} else {
            client.emit(videoAddEvent + err, "USER_NOT_CONNECTED");
	    client.emit(roomAskAuthEvent, "USER_NOT_CONNECTED");
	}
      });
  });
    client.on(videoDeleteEvent, function(videoID) {
	start = Date.now();
	Socket.isSocketIDConnected(client.id).then(value => {
	    if (value === true) {
		const roomID = client.id.substring(6, client.id.search("#"));
		Socket.getUserFromClientID(client.id).then(function(user) {
		    Playlist.isVideoExisting(roomID, videoID).then(function(isVideoExisting){
			if (isVideoExisting === true){
			    Playlist.DeleteVideo(roomID, videoID, user._id)
				.then(function(value) {
				    log.write(
					`${Timer.display(start)}[${client.id}]` +
					    ` ${user.username} ${videoDeleteEvent} ${videoID}`
				    );
				    client.emit(videoDeleteEvent + success, { id: videoID });
				    client.broadcast.emit(videoDeleteEvent, { id: videoID });
				})
				.catch(function(err) {
				    console.log(err);
				    client.emit(videoDeleteEvent + err, err);
				});
			}
			else
			    client.emit(videoDeleteEvent + err, {id: videoID, msg: "VIDEO_NOT_EXISTING"});
		    });
		});
	    } else {
		client.emit(videoDeleteEvent + err, "USER_NOT_CONNECTED");
		client.emit(roomAskAuthEvent, "USER_NOT_CONNECTED");
	    }
	});
    });
    client.on(videoAutoPlayEvent, function(autoplay) {
        start = Date.now();
        Socket.isSocketIDConnected(client.id).then(value => {
            if (value === true) {
                const roomID = client.id.substring(6, client.id.search("#"));
                Socket.getUserFromClientID(client.id).then(function(user) {
                    Playlist.setAutoPlay({roomID, userID: user._id, autoplay}).then(function(value) {
			log.write(
			    `${Timer.display(start)}[${client.id}]` +
				` ${user.username} ${videoAutoPlayEvent} ${autoplay}`
			);
                        client.emit(videoAutoPlayEvent + success, value);
                        client.broadcast.emit(videoAutoPlayEvent, value);
                    })
                        .catch(function(err) {
                            console.log(err);
                            client.emit(videoAutoPlayEvent + err, err);
                        });
                });
            } else {
                client.emit(videoAutoPlayEvent + err, "USER_NOT_CONNECTED");
		client.emit(roomAskAuthEvent, "USER_NOT_CONNECTED");
            }
        });
    });
    client.on(videoPlayStatusEvent, function(state) {
        start = Date.now();
        Socket.isSocketIDConnected(client.id).then(value => {
            if (value === true) {
                const roomID = client.id.substring(6, client.id.search("#"));
                Socket.getUserFromClientID(client.id).then(function(user) {
                    Playlist.setPlayerState({roomID, userID: user._id, state}).then(function(value) {
			log.write(
			    `${Timer.display(start)}[${client.id}]` +
				` ${user.username} ${videoPlayStatusEvent} ${value}`
			);
                        client.emit(videoPlayStatusEvent + success, value);
                        client.broadcast.emit(videoPlayStatusEvent, value);
                    }).catch(function(err) {
                            console.log(err);
                            client.emit(videoPlayStatusEvent + err, err);
                        });
                });
            } else {
                client.emit(videoPlayStatusEvent + err, "USER_NOT_CONNECTED");
		client.emit(roomAskAuthEvent, "USER_NOT_CONNECTED");
            }
        });
    });
    client.on(videoPlayerTimeEvent, function(time) {
        start = Date.now();
        Socket.isSocketIDConnected(client.id).then(value => {
            if (value === true) {
                const roomID = client.id.substring(6, client.id.search("#"));
                Socket.getUserFromClientID(client.id).then(function(user) {
                    Playlist.setPlayerTime({roomID, userID: user._id, time}).then(function(value) {
			log.write(
			    `${Timer.display(start)}[${client.id}]` +
				` ${user.username} ${videoPlayerTimeEvent} ${value}`
			);
                        client.emit(videoPlayerTimeEvent + success, value);
                        //client.broadcast.emit(videoPlayStatusEvent, value);
                    }).catch(function(err) {
                        console.log(err);
                        client.emit(videoPlayerTimeEvent + err, err);
                    });
                });
            } else {
                client.emit(videoPlayerTimeEvent + err, "USER_NOT_CONNECTED");
		client.emit(roomAskAuthEvent, "USER_NOT_CONNECTED");
            }
        });
    });
    client.on(videoOnSeekEvent, function(time) {
        start = Date.now();
        Socket.isSocketIDConnected(client.id).then(value => {
            if (value === true) {
                const roomID = client.id.substring(6, client.id.search("#"));
                Socket.getUserFromClientID(client.id).then(function(user) {
		    log.write(
			`${Timer.display(start)}[${client.id}]` +
			    ` ${user.username} ${videoOnSeekEvent} ${value}`
		    );
                    client.emit(videoOnSeekEvent + success, time);
                    client.broadcast.emit(videoOnSeekEvent, time);
                });
            } else {
                client.emit(videoPlayerTimeEvent + err, "USER_NOT_CONNECTED");
		client.emit(roomAskAuthEvent, "USER_NOT_CONNECTED");
            }
        });
    });
    client.on(roomSetPasswordEvent, function(password) {
        start = Date.now();
        Socket.isSocketIDConnected(client.id).then(value => {
            if (value === true) {
                const roomID = client.id.substring(6, client.id.search("#"));
                Socket.getUserFromClientID(client.id).then(function(user) {
                    Room.setPassword({roomID, userID: user._id, password}).then(function(value) {
			log.write(
			    `${Timer.display(start)}[${client.id}]` +
				` ${user.username} ${roomSetPasswordEvent} ${value}`
			);
                        client.emit(roomSetPasswordEvent + success, value);
                        client.broadcast.emit(roomSetPasswordEvent, value);
                    }).catch(function(err) {
                        console.log(err);
                        client.emit(roomSetPasswordEvent + err, err);
                    });
                });
            } else {
                client.emit(roomSetPasswordEvent + err, "USER_NOT_CONNECTED");
		client.emit(roomAskAuthEvent, "USER_NOT_CONNECTED");
            }
        });
    });
    client.on(roomSetIsPasswordProtectedEvent, function(state) {
        start = Date.now();
        Socket.isSocketIDConnected(client.id).then(value => {
            if (value === true) {
                const roomID = client.id.substring(6, client.id.search("#"));
                Socket.getUserFromClientID(client.id).then(function(user) {
		    console.log(state);
                    Room.setIsPasswordProtected({roomID, userID: user._id, state}).then(function(value) {
			log.write(
			    `${Timer.display(start)}[${client.id}]` +
				` ${user.username} ${roomSetIsPasswordProtectedEvent} ${value}`
			);
                        client.emit(roomSetIsPasswordProtectedEvent + success, value);
                        client.broadcast.emit(roomSetIsPasswordProtectedEvent, value);
                    }).catch(function(err) {
                        console.log(err);
                        client.emit(roomSetIsPasswordEvent + err, err);
                    });
                });
            } else {
                client.emit(roomSetIsPasswordProtectedEvent + err, "USER_NOT_CONNECTED");
		client.emit(roomAskAuthEvent, "USER_NOT_CONNECTED");
            }
        });
    });
    client.on(roomSetAdminPasswordEvent, function(password) {
        start = Date.now();
        Socket.isSocketIDConnected(client.id).then(value => {
            if (value === true) {
                const roomID = client.id.substring(6, client.id.search("#"));
                Socket.getUserFromClientID(client.id).then(function(user) {
                    Room.setAdminPassword({roomID, userID: user._id, password}).then(function(value) {
			log.write(
			    `${Timer.display(start)}[${client.id}]` +
				` ${user.username} ${roomSetAdminPassswordEvent} ${value}`
			);
                        client.emit(roomSetAdminPasswordEvent + success, value);
                        client.broadcast.emit(roomSetAdminPasswordEvent, value);
                    }).catch(function(err) {
                        console.log(err);
                        client.emit(roomSetAdminPasswordEvent + err, err);
                    });
                });
            } else {
                client.emit(roomSetAdminPasswordEvent + err, "USER_NOT_CONNECTED");
		client.emit(roomAskAuthEvent, "USER_NOT_CONNECTED");
            }
        });
    });
    client.on(videoMoveUpEvent, function(videoID) {
        start = Date.now();
        Socket.isSocketIDConnected(client.id).then(value => {
            if (value === true) {
                const roomID = client.id.substring(6, client.id.search("#"));
                Socket.getUserFromClientID(client.id).then(function(user) {
		    Playlist.moveUp({roomID, videoID, userID: user.id}).then(function(value){
			 log.write(
                            `${Timer.display(start)}[${client.id}]` +
                                ` ${user.username} ${videoMoveUpEvent} ${value}`
                        );
                        client.emit(videoMoveUpEvent + success, videoID);
                        client.broadcast.emit(videoMoveUpEvent, videoID);
		    }).catch(function(err){
			console.log(err);
			client.emit(videoMoveUpEvent + err, err);
		    });
		});
	    } else {
                client.emit(videoMoveUpEvent + err, "USER_NOT_CONNECTED");
                client.emit(roomAskAuthEvent, "USER_NOT_CONNECTED");
            }
	});
    });
    client.on(videoMoveDownEvent, function(videoID) {
        start = Date.now();
        Socket.isSocketIDConnected(client.id).then(value => {
            if (value === true) {
                const roomID = client.id.substring(6, client.id.search("#"));
                Socket.getUserFromClientID(client.id).then(function(user) {
                    Playlist.moveDown({roomID, videoID, userID: user.id}).then(function(value){
			 log.write(
                            `${Timer.display(start)}[${client.id}]` +
                                ` ${user.username} ${videoMoveDownEvent} ${value}`
                        );
                        client.emit(videoMoveDownEvent + success, videoID);
                        client.broadcast.emit(videoMoveDownEvent, videoID);
                    });
                });
            } else {
                client.emit(videoMoveDownEvent + err, "USER_NOT_CONNECTED");
                client.emit(roomAskAuthEvent, "USER_NOT_CONNECTED");
            }

        });
    });
};
