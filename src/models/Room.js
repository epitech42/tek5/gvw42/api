const db = require("../db/db.js");

const Video = new db.Schema({
    order: { type: Number, required: false},
    title: { type: String, required: false},
    url: { type: String, required: true },
    type:  { type: String, required: false },
    created: { type: Date, required: true, default: Date.now },
    updated: { type: Date, required: true, default: Date.now },
    createdBy: { type: String, required: true },
    updatedBy: { type: String, required: true }
});

const Room = db.model("Rooms", {
    title: { type: String, required: true },
    description: { type: String, required: false },
    administrators: { type: [String], required: false},
    password: { type: String, required: false },
    isPasswordProtected: { type: Boolean, required: false, default: false},
    passwordAdmin: { type: String, required: false },
    playlist: { type: [Video], required: false },
    usersConnected: { type: [String], required: true },
    miniature: { type: String, require: false },
    videoPlayer: {
	playerState: { type: String, required: true, default: false },
	playbackRate: { type: Number, requierd: true, default: 1.0},
	playerTime: { type: Number, required: true, default: 0 },
	autoPlay: { type: Boolean, required: true, default: false}
    },
    created: { type: Date, required: true, default: Date.now },
    updated: { type: Date, required: true, default: Date.now },
    createdBy: { type: String, required: true },
    updatedBy: { type: String, required: true },
    expireAt: { type: Date,  required: false }
});



module.exports = Room;


