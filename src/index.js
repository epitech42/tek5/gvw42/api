/* eslint global-require: "warn" */
// eslint-disable-line global-require
var morgan = require('morgan');

const log = require('./actions/notifications.js');

const io = require("socket.io")();
const redisAdapter = require("socket.io-redis");
const portSocket = process.env.PORT_SOCKET || 1337;

io.adapter(redisAdapter({ host: "redis", port: 6379 }));

/*
io.of("/").on("connect", socket => {
  console.log(`New Client ${socket.id} on AutoComplete connected on WebSocket`);
  require("./socket/event/autocomplete.js")(socket);
});
*/
io.of(/^\/room-(.)+$/).on("connect", socket => {
    log.write(`New Client ${socket.id} connected on WebSocket`);
    require("./socket/event/room.js")(socket);
    require("./socket/event/video.js")(socket);
});

//io.use(morgan("short"));
io.listen(portSocket);

io.eio.pingTimeout = 60000; // 2 minutes
io.eio.pingInterval = 5000;  // 5 seconds

// EXPRESS API REST
const express = require("express");
const cors = require("cors");

const port = process.env.PORT_REST || 3000;
const app = express();

// Import Swagger
const swaggerUi = require("swagger-ui-express");
const swaggerJSDoc = require("swagger-jsdoc");

// Decode body request with json
if (process.env.UNIT_TEST !== "true")
    app.use(morgan("short"));

app.use(express.json());

app.options("*", cors());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "X-Requested-With",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

require("./routes/user.js")(app, cors);
require("./routes/room.js")(app, cors);

const swaggerDefinition = {
  info: {
    title: "GVW API",
    version: "1.0.0",
    description: "Docs to use GVW API"
  },
  // host: 'localhost:3000',
  basePath: "/"
};

// options for the swagger docs
const options = {
  // import swaggerDefinitions
  swaggerDefinition,
  // path to the API docs
  apis: ["index.js", "routes/*.js"] // pass all in array
};

if (process.env.name === "dev"){
    var length = options.apis.length
    for (var i = 0;i <= length; i+=1){
	options.apis[i] = "src/" + options.apis[i]
    }
}
// initialize swagger-jsdoc
const swaggerSpec = swaggerJSDoc(options);

app.get("/", (req, res) => res.send("GVW API"));
app.get("/swagger.json", function(req, res) {
  res.setHeader("Content-Type", "application/json");
  res.send(swaggerSpec);
});
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.get("/health", function(req, res) {
  res.setHeader("Content-Type", "application/json");
  res.send({ msg: "I'm alive" });
});

app.listen(port, () =>
  console.log(`GVW API REST started\nListening on port ${port} !`)
);

/**
 * @swagger
 * securityDefinitions:
 *   ApiKeyAuth:        # arbitrary name for the security scheme
 *     type: apiKey
 *     in: header       # can be "header", "query" or "cookie"
 *     name: token 
 * definitions:
 *   users:
 *     properties:
 *         type: array
 *         items:
 *           type: string
 *   user:
 *     properties:
 *       username:
 *         type: string
 *       password:
 *         type: string
 *   userLogin:
 *     properties:
 *       token:
 *         type: string
 *   userDestroy:
 *     properties:
 *       username:
 *         type: string
 *       destroyPassphrase:
 *         type: string
 *   userCreated:
 *    properties:
 *      msg:
 *        type: string
 *      username:
 *        type: string
 *   projectCreate:
 *    properties:
 *     url:
 *        type: string
 *     title:
 *        type: string
 *   projectCreated:
 *    properties:
 *      msg:
 *        type: string
 *      projectID:
 *        type: string
 *   error:
 *    properties:
 *      err:
 *        type: string
 *   projectLogin:
 *    properties:
 *      projectID:
 *        type: string
 *   projectLoginSuccess:
 *    properties:
 *      token:
 *        type: string
 *   projectAddUsers:
 *     properties:
 *       projectID:
 *         type: string
 *       users:
 *         type: array
 *         items:
 *           type: string
 */

module.exports = app;
