// Import Node Modules
const { check, validationResult } = require("express-validator");

// Import Actions
const User = require("../actions/user.js");
const Room = require("../actions/room.js");

// Import Misc Actions
const Timer = require("../actions/timer.js");
const log = require("../actions/notifications.js");

let start = new Date();

/* ROUTES */
const registerRoute = "/room/create";
const loginRoute = "/room/login";
const editRoomRoute = "/room/edit";
const destroyRoute = "/room/delete";
const getUserRoute = "/room/get";
const listRoomsRoute = "/room/list";
const getRoomRoute = "/room/get/:id";

module.exports = function (app, cors) {
  /**
   * @swagger
   * /room/create:
   *   post:
   *     tags:
   *       - room
   *     description: Creates a new room
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: room
   *         description: room to create
   *         in: body
   *         required: true
   *         schema:
   *           $ref: '#/definitions/room'
   *     responses:
   *       201:
   *         description: User created
   *         schema:
   *           $ref: '#/definitions/roomCreated'
   *       422:
   *         description: Error Code
   *         schema:
   *           $ref: '#/definitions/error'
   */
  app.post(
    registerRoute,
    [
	check("title").exists(),
	check("token").exists(),
	check("token", "TOKEN_ERROR").custom(token => {
            return User.verifyToken(token).then(value => {
		if (value === false)
		    throw false;
		return true;
            });
	}),
	check("token", "USER_NOT_FOUND").custom(token => {
            return User.getUserfromToken(token).then(user => {
		return User.isUsernameExist(user.username).then(value => {
		    if (value === false)
			throw false;
		    return true;
		});
            });
	})
    ],
    cors(),
    function (req, res) {
      start = Date.now();
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        log.write(
          `${Timer.display(start) + registerRoute} ${JSON.stringify(
            errors.array(),
            null,
            4
          )}`
        );
        res.status(422).json({ errors: errors.array() });
      }
	else {
	    log.write(
		`${registerRoute} ` +
		    `Creating Room: ${JSON.stringify(req.body, null, 4)}`
	    );
	    User.getUserfromToken(req.headers.token).then(function (user) {
		Room.create(user.id, req.body.title, req.body.description)
		    .then(function (value) {
			res.status(201).json(value);
			log.write(
			    `${Timer.display(start) + registerRoute} ${req.body.title} Created`
			);
		    })
		    .catch(function (CreateErr) {
			console.log(CreateErr.message);
			res.status(400).json({ err: CreateErr.message });
			log.write(
			    `${Timer.display(start) + registerRoute} ${req.body.title} ${ CreateErr.message }`
			);
			log.write(
			    `${Timer.display(start) + registerRoute} ${ req.body.title } not Created`
			);
		    });
	    });
	}
    }
  );
  /**
   * @swagger
   * /room/login:
   *   post:
   *     tags:
   *       - room
   *     description: Login as user on room
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: user
   *         description: user to login
   *         in: body
   *         required: true
   *         schema:
   *           $ref: '#/definitions/user'
   *     responses:
   *       201:
   *         description: User login
   *         schema:
   *           $ref: '#/definitions/userLogin'
   *       422:
   *         description: Error Code
   *         schema:
   *           $ref: '#/definitions/error'
   */
  app.post(
    loginRoute,
      [
	  check("token").exists(),
	  check("token", "TOKEN_ERROR").custom(token => {
              return User.verifyToken(token).then(value => {
		  if (value === false)
		      throw false;
		  return true;
              });
	  }),
	  check("token", "USER_NOT_FOUND").custom(token => {
              return User.getUserfromToken(token).then(user => {
		  return User.isUsernameExist(user.username).then(value => {
		      if (value === false)
			  throw false;
		      return true;
		  });
              });
	  }).bail(),
          check("id").exists(),
	  check("id", "ROOM_NOT_FOUND").custom(id => {
              return Room.isRoomExist(id).then(value => {
                  if (value === false)
                      throw false;
	          return true;
              });
          }).bail(),
	  check("password", "WRONG_PASSWORD").custom((password, {req}) => {
	      return Room.isPasswdExist(req.body.id).then(value => {
		  if (value === true){
		      if (password === undefined || password === null)
			  throw "PASSWORD_IS_NEEDED";
		      else{
			  return Room.cmpPasswd(req.body.id, password, "room").then(value => {
			      if (value === false)
				  throw false;
			      else
				  return true;
			  });
		      }
		  }
		  else
		      return true;
	      });
	  })
      ],
	  cors(),
    function (req, res) {
      start = Date.now();
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        log.write(
          `${Timer.display(start) + registerRoute} ${JSON.stringify(
            errors.array(),
            null,
            4
          )}`
        );
        res.status(422).json({ errors: errors.array() });
      }
	else{
	    log.write(
		`${loginRoute} Login User: ${JSON.stringify(req.body, null, 4)}`
	    );
	    User.getUserfromToken(req.headers.token).then(user => {
		Room.generateToken(user, req.body.id, req.body.password)
		    .then(function (value) {
			res.status(200).json({"token": value});
			log.write(`${Timer.display(start) + loginRoute} ${value}`);
		    })
		    .catch(function (err) {
			console.log(err);
			res.status(400).json({ err: err.message });
		    });
	    });
	}
    }
  );

  /**
   * @swagger
   * /rooms/list:
   *   get:
   *     tags:
   *       - rooms
   *     description: list
   *     security:
   *       - ApiKeyAuth: []
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Rooms Array
   *       422:
   *         description: Error Code
   *         type: string
   *
   */
  app.get(
    listRoomsRoute,
    [
      check("token").exists(),
      check("token", "TOKEN_ERROR").custom(token => {
        return User.verifyToken(token).then(value => {
          if (value === false)
            throw false;
          return true;
        });
      }),
      check("token", "USER_NOT_FOUND").custom(token => {
        return User.getUserfromToken(token).then(user => {
          return User.isUsernameExist(user.username).then(value => {
            if (value === false)
              throw false;
            return true;
          });
        });
      })
    ],
    cors(),
    function (req, res) {
      start = Date.now();
      try {
        validationResult(req).throw();

        log.write(
          `${getUserRoute} getListRooms: ${JSON.stringify(req.body, null, 4)}`
        );
          User.getUserfromToken(req.headers.token).then(function (user) {
              Room.list(user.id).then(function (value) {
		  res.json(value);
		  log.write(
		      `${Timer.display(start) + listRoomsRoute} ${
              user.username
              } ${value}`
		  );
              }).catch(function (err) {
		  //console.log(err);
		  res.status(400).json({ err: err.message });
              });
        }).catch(function (err) {
          console.log(err);
          res.status(400).json({ err: err.message });
        });
      } catch (err) {
          if (!err.isEmpty()) {
              log.write(
		  `${Timer.display(start) + getUserRoute} ${JSON.stringify(
              err.array(),
              null,
              4
            )}`
              );
              res.status(422).json({ errors: err.array() });
          }
      }
    }
  );

  /**
   * @swagger
   * /rooms/get:
   *   get:
   *     tags:
   *       - rooms
   *     description: list
   *     security:
   *       - ApiKeyAuth: []
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Room
   *       422:
   *         description: Error Code
   *         type: string
   *
   */
  app.get(
    getRoomRoute,
    [
      check("token").exists(),
      check("token", "TOKEN_ERROR").custom(token => {
        return User.verifyToken(token).then(value => {
          if (value === false)
            throw false;
          return true;
        });
      }),
      check("token", "USER_NOT_FOUND").custom(token => {
        return User.getUserfromToken(token).then(user => {
          return User.isUsernameExist(user.username).then(value => {
            if (value === false)
              throw false;
            return true;
          });
        });
      })
    ],
    cors(),
    function (req, res) {
      start = Date.now();
      try {
        validationResult(req).throw();

        log.write(
          `${getRoomRoute}: ${JSON.stringify(req.body, null, 4)}`
        );
          User.getUserfromToken(req.headers.token).then(function (user) {
              Room.getRoomFromID(req.params.id).then(function (value) {
		  res.json(value);
		  log.write(
		      `${Timer.display(start) + getRoomRoute} ${
              user.username
              } ${value}`
		  );
              }).catch(function (err) {
		  //console.log(err);
		  res.status(400).json({ err: err.message });
              });
          }).catch(function (err) {
              console.log(err);
              res.status(400).json({ err: err.message });
          });
      } catch (err) {
          if (!err.isEmpty()) {
              log.write(
		  `${Timer.display(start) + getRoomRoute} ${JSON.stringify(
              err.array(),
              null,
              4
            )}`
              );
              res.status(422).json({ errors: err.array() });
          }
      }
    }
  );

    
  /**
   * @swagger
   * /room/edit:
   *   put:
   *     tags:
   *       - room
   *     description: room
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Room Editing
   *       422:
   *         description: Error Code
   *         type: string
   *
   */
  app.put(
    editRoomRoute,
    [
      check("token").exists(),
      check("token", "TOKEN_ERROR").custom(token => {
        return User.verifyToken(token).then(value => {
          if (value === false)
            throw false;
          return true;
        });
      }),
      check("token", "USER_NOT_FOUND").custom(token => {
        return User.getUserfromToken(token).then(user => {
          return User.isUsernameExist(user.username).then(value => {
            if (value === false)
              throw false;
            return true;
          });
        });
      }),
	check("id").exists(),
	check("id", "ROOM_NOT_FOUND").custom(id => {
            return Room.isRoomExist(id).then(value => {
                if (value === false)
                    throw false;
	        return true;
            });
        }),
	check("id", "USER_NO_ADMIN_PERMISSIONS").custom((id, { req }) => {
		return User.getUserfromToken(req.headers.token).then(user => {
		    return Room.isUserAdminOfRoom(user.id, id).then(value => {
			//console.log(value);
			if (value === false)
			{
			    return Room.isPasswdAdminExist(id).then(value => {
				if (value === false)
				    throw "USER_NO_ADMIN_PERMISSIONS";
				else{
				    if (req.body.passwordAdmin !== undefined){
					return Room.cmpPasswd(id, req.body.passwordAdmin, "admin").then(value => {
					    if (value === false)
						throw "WRONG_PASSWORD";
					    return true;
					});
				    }
				    else
					throw "PASSWORD_NEEDED";
				}
			    });
			}
		    });
		});
	})
    ],
    cors(),
    function (req, res) {
      start = Date.now();
      try {
        validationResult(req).throw();
        User.getUserfromToken(req.headers.token).then(function (user) {
            Room.edit(user.id, req.body.id, req.body.title, req.body.description, req.body.password, req.body.isPasswordProtected, req.body.newPasswordAdmin).then(function (value) {
		res.json({
		    msg: value
		});
		log.write(
		    `${Timer.display(start) + editRoomRoute} ${
              user.username
              } ${JSON.stringify(value, null, 4)}`
		);
          });
        });
      } catch (err) {
        if (!err.isEmpty()) {
          log.write(
            `${Timer.display(start) + editRoomRoute} ${JSON.stringify(
              err.array(),
              null,
              4
            )}`
          );
          res.status(422).json({ errors: err.array() });
        }
      }
    }
  );

  /**
   * @swagger
   * /room/delete:
   *   delete:
   *     tags:
   *       - room
   *     description: Delete room
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Room deleted
   *       422:
   *         description: Error Code
   *         type: string
   *
   */
  app.delete(
    destroyRoute,
      [
	  check("token").exists(),
	  check("token", "TOKEN_ERROR").custom(token => {
              return User.verifyToken(token).then(value => {
		  if (value === false)
		      throw false;
		  else
		      return true;
              });
	  }),
	  check("token", "USER_NOT_FOUND").custom(token => {
              return User.getUserfromToken(token).then(user => {
		  return User.isUsernameExist(user.username).then(value => {
		      if (value === false)
			  throw false;
		      else
			  return true;
		  });
              });
	  }).bail(),
	  check("id").exists(),
	  check("id", "ROOM_NOT_FOUND").custom(id => {
	      return Room.isRoomExist(id).then(value => {
		  if (value === false)
		      throw false;
		  else
		      return true;
	      });
	  }).bail(),
	  check("id", "USER_NO_ADMIN_PERMISSIONS").custom((id, { req }) => {
	      return User.getUserfromToken(req.headers.token).then(user => {
		  return Room.isUserAdminOfRoom(user.id, id).then(isAdmin => {
		      if (isAdmin === false)
		      {
			  return Room.isPasswdAdminExist(id).then(isPasswdAdmin => {
			      if (isPasswdAdmin === false)
				  throw "USER_NO_ADMIN_PERMISSIONS";
			      else{
				  if (req.body.passwordAdmin !== undefined){
				      return Room.cmpPasswd(id, req.body.passwordAdmin, "admin").then(value => {
					  if (value === false)
					      throw "WRONG_PASSWORD";
					  else
					      return true;
				      });
				  }
				  else
				      throw "PASSWORD_NEEDED";
			      }
			  });
		      }
		      else
			  return true;
		  });
	      });
	  })
      ],
    cors(),
      function (req, res) {
	  start = Date.now();
	  const errors = validationResult(req);
	  if (!errors.isEmpty()) {
              log.write(
		  `${Timer.display(start) + registerRoute} ${JSON.stringify(errors.array(), null, 4)}`
              );
              res.status(422).json({ errors: errors.array() });
	  }
          else{
              log.write(
		  `${destroyRoute} ` +
		      `Deleting Room: ${JSON.stringify(req.body, null, 4)}`
              );
              User.getUserfromToken(req.headers.token).then(function (user) {
		  Room.delete(req.body.id).then(function (value) {
		      res.json({
			  message: value,
			  roomID: req.body.id
		      });
		      log.write(
			  `${Timer.display(start) + destroyRoute} ${req.body.id} ${value}`
		      );
		  }).catch(function (err) {
		      console.log(err);
		      res.status(400).json({ err: err.message });
		  });
              });
	  }
      }
  );
};
