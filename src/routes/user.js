// Import Node Modules
const { check, validationResult } = require("express-validator");

// Import Actions
const User = require("../actions/user.js");

// Import Misc Actions
const Timer = require("../actions/timer.js");
const log = require("../actions/notifications.js");

let start = new Date();

/* ROUTES */
const registerRoute = "/user/register";
const loginRoute = "/user/login";
const destroyRoute = "/user/delete";
const getUserRoute = "/user/get";
const editUserRoute = "/user/edit";

module.exports = function (app, cors) {
  /**
   * @swagger
   * /user/register:
   *   post:
   *     tags:
   *       - user
   *     description: Creates a new user
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: user
   *         description: user to create
   *         in: body
   *         required: true
   *         schema:
   *           $ref: '#/definitions/user'
   *     responses:
   *       201:
   *         description: User created
   *         schema:
   *           $ref: '#/definitions/userCreated'
   *       422:
   *         description: Error Code
   *         schema:
   *           $ref: '#/definitions/error'
   */
  app.post(
    registerRoute,
    [
      check("username").exists(),
      check("password").exists(),
      check("username", "USERNAME_NOT_VALID").custom(username => {
	  if ( /^[a-zA-Z0-9_-]*$/g.test(username) === false)
              throw false;
          return true;
      }),
	check("username", "USERNAME_ALREADY_EXISTING").custom(username => {
            return User.isUsernameExist(username).then(value => {
		if (!value === false)
                    throw false;
                return true;
            });
	})
    ],
    cors(),
    function (req, res) {
      start = Date.now();
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        log.write(
          `${Timer.display(start) + registerRoute} ${JSON.stringify(
            errors.array(),
            null,
            4
          )}`
        );
	  log.write(
	      `${Timer.display(start) + registerRoute} ${
            req.body.username
            } not Created`
	  );
          res.status(422).json({ errors: errors.array() });
      }
	else{
	    log.write(
		`${registerRoute} ` +
		    `Creating User: ${JSON.stringify(req.body, null, 4)}`
	    );
	    User.create(req.body.username, req.body.password)
		.then(function (value) {
		    res.status(201).json(value);
		    log.write(
			`${Timer.display(start) + registerRoute} ${value.username} Created`
		    );
		});
	}
    }
  );
  /**
   * @swagger
   * /user/login:
   *   post:
   *     tags:
   *       - user
   *     description: Login as user
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: user
   *         description: user to login
   *         in: body
   *         required: true
   *         schema:
   *           $ref: '#/definitions/user'
   *     responses:
   *       200:
   *         description: User login
   *         schema:
   *           $ref: '#/definitions/userLogin'
   *       422:
   *         description: Error Code
   *         schema:
   *           $ref: '#/definitions/error'
   */
  app.post(
    loginRoute,
    [
	check("username").exists(),
	check("password").exists(),
	check("username", "USERNAME_NOT_FOUND").custom(username => {
            return User.isUsernameExist(username).then(value => {
		if (value === false)
		    throw false;
		return true;
            });
	}),
	check("password", "WRONG_PASSWORD").custom((password, {req}) => {
	    return User.cmpPasswd(req.body.username, password).then(value => {
		if (value === false)
		    throw false;
	    });
	})
    ],
    cors(),
    function (req, res) {
      start = Date.now();
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        log.write(
          `${Timer.display(start) + registerRoute} ${JSON.stringify(
            errors.array(),
            null,
            4
          )}`
        );
        res.status(422).json({ errors: errors.array() });
      }
	else{
	    log.write(
		`${loginRoute} Login User: ${JSON.stringify(req.body, null, 4)}`
	    );
	    User.login(req.body.username, req.body.password)
		.then(function (value) {
		    res.status(200).json(value);
		    log.write(`${Timer.display(start) + loginRoute} ${value}`);
		})
		.catch(function (err) {
		    console.log(err);
		    res.status(400).json({ err: err.message });
		});
	}
    }
  );

  /**
   * @swagger
   * /user/get:
   *   get:
   *     tags:
   *       - user
   *     description: user
   *     security:
   *       - ApiKeyAuth: []
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: User
   *       422:
   *         description: Error Code
   *         type: string
   *
   */
  app.get(
    getUserRoute,
    [
      check("token").exists(),
      check("token", "TOKEN_ERROR").custom(token => {
        return User.verifyToken(token).then(value => {
          if (value === false)
            throw false;
          return true;
        });
      }),
      check("token", "USER_NOT_FOUND").custom(token => {
          return User.getUserfromToken(token).then(user => {
              return User.isUsernameExist(user.username).then(value => {
		  if (value === false)
		      throw false;
		  return true;
              });
          });
      })
    ],
    cors(),
    function (req, res) {
      start = Date.now();
      try {
        validationResult(req).throw();

        log.write(
          `${getUserRoute} getUser: ${JSON.stringify(req.body, null, 4)}`
        );
        User.getUserfromToken(req.headers.token).then(function (user) {
          User.getUserFromID(user.id).then(function (value) {
            res.json(value);
            log.write(
              `${Timer.display(start) + getUserRoute} ${
              req.headers.token
              } ${value}`
            );
          }).catch(function (err) {
            console.log(err);
            res.status(400).json({ err: err.message });
          });
        }).catch(function (err) {
          console.log(err);
          res.status(400).json({ err: err.message });
        });
      } catch (err) {
        if (!err.isEmpty()) {
          log.write(
            `${Timer.display(start) + getUserRoute} ${JSON.stringify(
              err.array(),
              null,
              4
            )}`
          );
          res.status(422).json({ errors: err.array() });
        }
      }
    }
  );

  /**
   * @swagger
   * /user/edit:
   *   put:
   *     tags:
   *       - user
   *     description: user
   *     security:                                                                                                                                                                                                
   *       - ApiKeyAuth: [] 
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: User
   *       422:
   *         description: Error Code
   *         type: string
   *
   */
  app.put(
    editUserRoute,
    [
	check("token").exists(),
	check("token", "TOKEN_ERROR").custom(token => {
            return User.verifyToken(token).then(value => {
		if (value === false)
		    throw false;
		else
		    return true;
            });
	}),
	check("token", "USER_NOT_FOUND").custom(token => {
            return User.getUserfromToken(token).then(user => {
		return User.isUsernameExist(user.username).then(value => {
		    if (value === false)
			throw false;
		    else
			return true;
		});
            });
	}),
	check("username", "USERNAME_NOT_VALID").custom(username => {
	    if (/^[a-zA-Z0-9_-]*$/g.test(username) === false)
                throw false;
	    else
		return true;
	}),
	check("username", "USERNAME_ALREADY_EXISTING").custom((username, {req})  => {
	    return User.getUserfromToken(req.headers.token).then(user => {	    
		return User.isUsernameExist(username).then(value => {
		    if (!value === false && username !== user.username)
			throw false;
		    else
			return true;
		});
	    });
	})
    ],
      cors(),
      function (req, res) {
      start = Date.now();
      try {
        validationResult(req).throw();
	  
          User.getUserfromToken(req.headers.token).then(function (user) {
              User.edit(user.id, req.body.username, req.body.password).then(function (value) {
		  res.json({
		      msg: value
		  });
		  
		  log.write(
		      `${Timer.display(start) + editUserRoute} ${
              user.username
              } ${JSON.stringify(value, null, 4)}`
		  );
              });
          });
      } catch (err) {
        if (!err.isEmpty()) {
          log.write(
            `${Timer.display(start) + editUserRoute} ${JSON.stringify(
              err.array(),
              null,
              4
            )}`
          );
          res.status(422).json({ errors: err.array() });
        }
      }
    }
  );

  /**
   * @swagger
   * /user/delete:
   *   delete:
   *     tags:
   *       - user
   *     description: Delete user
   *     security:                                                                                                                                                                                                
   *       - ApiKeyAuth: [] 
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: User deleted
   *       422:
   *         description: Error Code
   *         type: string
   *
   */
  app.delete(
    destroyRoute,
    [
	check("token").exists(),
	check("token", "TOKEN_ERROR").custom(token => {
            return User.verifyToken(token).then(value => {
		if (value === false)
		    throw false;
		else
		    return true;
            });
	}),
	check("token", "USER_NOT_FOUND").custom(token => {
            return User.getUserfromToken(token).then(user => {
		return User.isUsernameExist(user.username).then(value => {
		    if (value === false)
			throw false;
		    else
			return true;
          });
        });
      })
    ],
    cors(),
    function (req, res) {
      start = Date.now();
      try {
        validationResult(req).throw();

        log.write(
          `${destroyRoute} ` +
          `Deleting User: ${JSON.stringify(req.body, null, 4)}`
        );
        User.getUserfromToken(req.headers.token).then(function (user) {
          User.delete(user.id).then(function (value) {
            res.json({
              message: value,
              username: user.username
            });
            log.write(
              `${Timer.display(start) + destroyRoute} ${req.body.username} ${value}`
            );
          }).catch(function (err) {
            console.log(err);
            res.status(400).json({ err: err.message });
          });
        });
      } catch (err) {
        if (!err.isEmpty()) {
          log.write(
            `${Timer.display(start) + destroyRoute} ${JSON.stringify(
              err.array(),
              null,
              4
            )}`
          );
          res.status(422).json({ errors: err.array() });
        }
      }
    }
  );
};
