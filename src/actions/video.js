// Import Node Modules
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// Import User Model
const UserModel = require("../models/User.js");
const RoomModel = require("../models/Room.js");

const cache = require("../db/cache.js").client;

const { promisify } = require("util");

const getAsync = promisify(cache.get).bind(cache);

const { ObjectID } = require("mongodb");

module.exports = {
    AddVideo(roomID, userID, url) {
	return new Promise(function(resolve, reject) {
	    RoomModel.find({ _id: roomID }, function(error, rooms) {
		if (rooms[0] !== undefined) {
		    const _id = new ObjectID();
		    rooms[0].playlist.push({
			_id,
			url,
			//order: rooms[0].playlist.length,
			createdBy: userID,
			updatedBy: userID
		    });
		    rooms[0].save().then(function(value) {
			cache.set(roomID, JSON.stringify(rooms[0]));
			resolve(value.playlist.find(video => video._id === _id));
		    });
		} else {
		    reject(new Error("ROOM_NOT_FOUND"));
		}
	    });
	});
    },
    EditVideo(roomID, userID, videoID) {
	return new Promise(function(resolve, reject) {
	    RoomsModel.updateMany(
		{ _id: roomID, "subtitles._id": subtitleID },
		{
		    $set: { "subtitles.$.beginTime": beginTime, "subtitles.$.endTime": endTime, "subtitles.$.content": content, "subtitles.$.updatedBy": updatedBy, "subtitles.$.updated": new Date(Date.now()).toISOString() }
		},
		function(error, value) {
		    resolve(
			//value.subtitles.find(subtitle => subtitle._id === subtitleID)
			value
		    );
		}
	    );
	});
    },
    isVideoExisting(roomID, videoID){
	return new Promise(function(resolve, reject){
	    cache.get(roomID, (err, result) => {
		if (result !== undefined && result !== null) {
		    var room = JSON.parse(result);
		    var result = room.playlist.find(video => {
			return video._id === videoID
		    })
		    if (result !== undefined)
			resolve(true);
		    else
			resolve(false);
		}
		else {
		    
		}
	    });
	});
    },
    DeleteVideo(roomID, videoID) {
	return new Promise(function(resolve, reject) {
	    cache.get(roomID, (err, result) => {
                if (result !== undefined && result !== null) {
                    var room = JSON.parse(result);
		    room.playlist = room.playlist.filter(function(obj){
			return obj._id !== videoID;
		    });
		    cache.set(roomID, JSON.stringify(room));
		}
	    });
	    RoomModel.find({ _id: roomID }, function(error, rooms) {
		if (rooms !== undefined) {
		    RoomModel.updateOne(
			{
			    _id: roomID,
			    "playlist._id": videoID
			},
			{
			    $pull: { playlist: { _id: videoID }}
			},
			function(error, result) {
			    resolve(result);
			}
		    );
		} else {
		    reject(new Error("ROOM_NOT_FOUND"));
		}
	    });
	});
    },
    setAutoPlay({roomID, userID, autoplay}){
        return new Promise(function(resolve, reject){
            cache.get(roomID, (err, result) => {
                if (result !== undefined && result !== null) {
                    //resolve(true);                                                                                                                                                                              
                    var room = JSON.parse(result);
                    room.videoPlayer.autoPlay = autoplay;
                    cache.set(roomID, JSON.stringify(room));
                }
                RoomModel.updateMany(
                    { _id: roomID },
                    {
                        $set: { "videoPlayer.autoPlay": autoplay, "updatedBy": userID, "updated": Date.now() }
                    }).then(function(error, comments){
                        //console.log(comments);
			resolve(autoplay);
                    });
            });
        });
    },
    setPlayerState({roomID, userID, state}){
	return new Promise(function(resolve, reject){
            cache.get(roomID, (err, result) => {
                if (result !== undefined && result !== null) {
                    var room = JSON.parse(result);
                    room.videoPlayer.playerState = state;
                    cache.set(roomID, JSON.stringify(room));
                }
                RoomModel.updateMany(
                    { _id: roomID },
                    {
                        $set: { "videoPlayer.playerState": state, "updatedBy": userID, "updated": Date.now() }
                    }).then(function(error, comments){
                        //console.log(comments);
			resolve(state);
                    });
            });
        });
    },
    setPlayerTime({roomID, userID, time}){
	return new Promise(function(resolve, reject){
            cache.get(roomID, (err, result) => {
                if (result !== undefined && result !== null) {
                    var room = JSON.parse(result);
                    room.videoPlayer.playerTime = time;
		    cache.set(roomID, JSON.stringify(room));
                }
                RoomModel.updateMany(
                    { _id: roomID },
                    {
                        $set: { "videoPlayer.playerTime": time, "updatedBy": userID, "updated": Date.now() }
                    }).then(function(error, comments){
                        //console.log(comments);
			resolve(time);
                    });
            });
        });
    },
    moveUp({roomID, videoID, userID}){
	return new Promise(function(resolve, reject){
	    cache.get(roomID, (err, result) => {
		if (result !== undefined && result !== null) {
                    var room = JSON.parse(result);
		    var videoIndex = room.playlist.findIndex(video => video._id.toString() === videoID);
		    var tmp = room.playlist[videoIndex];

		    if (room.playlist.length > 1){
		    room.playlist[videoIndex] = room.playlist[videoIndex - 1];
		    room.playlist[videoIndex - 1] = tmp;
		    
		    room.playlist[videoIndex].updatedBy = userID;
		    room.playlist[videoIndex].updated = Date.now();
		    
		    room.playlist[videoIndex - 1].updatedBy = userID;
		    room.playlist[videoIndex - 1].updated = Date.now();

		    cache.set(roomID, JSON.stringify(room));
			resolve(videoID);
		    }
		    else
			reject(videoID);
		}
		RoomModel.find({ _id: roomID }, function(error, comments){
		    if (comments !== undefined && comments.length > 0)
		    {
			var room = comments[0];
			var videoIndex = room.playlist.findIndex(video => video._id.toString() === videoID);
			var tmp = room.playlist[videoIndex];

			if (room.playlist.length > 1){
			room.playlist[videoIndex] = room.playlist[videoIndex - 1];
			room.playlist[videoIndex - 1] = tmp;
			
			room.playlist[videoIndex].updatedBy = userID;
			room.playlist[videoIndex].updated = Date.now();

			room.playlist[videoIndex - 1].updatedBy = userID;
			room.playlist[videoIndex - 1].updated = Date.now();			
			RoomModel.updateMany(
			    { _id: roomID },
			    {
                     		$set: { "playlist": room.playlist, "updatedBy": userID, "updated": Date.now() }
			    }).then(function(error, comments){
				//console.log(comments);                                                                                                                                                          
				resolve(videoID);
			    });
			}
			else
			    reject(videoID);
		    }
		});
	    });
	});
    },
    moveDown({roomID, videoID, userID}){
	return new Promise(function(resolve, reject){
	    cache.get(roomID, (err, result) => {
		if (result !== undefined && result !== null) {
                    var room = JSON.parse(result);
		    var videoIndex = room.playlist.findIndex(video => video._id.toString() === videoID);
		    var tmp = room.playlist[videoIndex];
		    room.playlist[videoIndex] = room.playlist[videoIndex + 1];
		    room.playlist[videoIndex + 1] = tmp;
		    
		    room.playlist[videoIndex].updatedBy = userID;
		    room.playlist[videoIndex].updated = Date.now();
		    
		    room.playlist[videoIndex + 1].updatedBy = userID;
		    room.playlist[videoIndex + 1].updated = Date.now();

		    cache.set(roomID, JSON.stringify(room));
		    resolve(videoID);
		}
		RoomModel.find({ _id: roomID }, function(error, comments){
		    if (comments !== undefined && comments.length > 0)
		    {
			var room = comments[0];
			var videoIndex = room.playlist.findIndex(video => video._id.toString() === videoID);
			var tmp = room.playlist[videoIndex];
			room.playlist[videoIndex] = room.playlist[videoIndex + 1];
			room.playlist[videoIndex + 1] = tmp;
			
			room.playlist[videoIndex].updatedBy = userID;
			room.playlist[videoIndex].updated = Date.now();
			
			room.playlist[videoIndex + 1].updatedBy = userID;
			room.playlist[videoIndex + 1].updated = Date.now();			

			RoomModel.updateMany(
			    { _id: roomID },
			    {
                     		$set: { "playlist": room.playlist, "updatedBy": userID, "updated": Date.now() }
			    }).then(function(error, comments){
				//console.log(comments);                                                                                                                                                          
				resolve(videoID);
			    });
		    }
		});
	    });
	});
    }
};
