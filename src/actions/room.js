// Import Node Modules
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const User = require("./user.js");

// Import User Model
const UserModel = require("../models/User.js");
const RoomsModel = require("../models/Room.js");

const cache = require("../db/cache.js").client;

//const { promisify } = require("util");

//const getAsync = promisify(cache.get).bind(cache);

const Room = {
    async create(userID, title, description) {
    return new Promise(function(resolve, reject) {
      const room = new RoomsModel({
        title,
        description,
        createdBy: userID,
        updatedBy: userID
      });
	room.administrators.push(userID);
	room
            .save()
            .then(() => {
		cache.set(room._id.toString(), JSON.stringify(room));
		resolve({
		    message: "ROOM_CREATED",
		    roomID: room._id
		});
            })
        .catch(function(err) {
          reject(new Error(err));
        });
    });
  },
  getRoomFromID(id) {
    return new Promise(function(resolve, reject) {
	cache.get(id, function(err, value) {
            if (value !== undefined && value !== null) {
		console.log("rooms in redis");
		resolve(JSON.parse(value));
            } else {
		RoomsModel.find({ _id: id }, function(error, comments) {
		    if (comments[0] !== undefined || comments[0] !== null) {
			console.log("rooms in mongo");
			resolve(comments[0]);
		    } else reject(new Error("ROOM_NOT_FOUND"));
		});
            }
	});
    }).catch(function(err) {
	throw new Error(err.message);
    });
  },
    delete(roomID) {
	return new Promise(function(resolve, reject) {
	    RoomsModel.find({ _id: roomID }, function(error, comments) {
		if (comments === 0)
		    reject(new Error("ROOM_NOT_FOUND"));
		else {
		    cache.del(roomID, function (err, response) {
			//console.log("delete in redis " + response);
			RoomsModel.deleteOne({ _id: roomID }, function(error, comments) {
			    //console.log(`${JSON.stringify(comments, null, 4)}`);
			    resolve("ROOM_DELETED");
			});
		    });
		}
	    });
	}).catch(function(err) {
	    throw new Error(err);
	});
    },
    isRoomExist(id) {
	return new Promise(function(resolve, reject) {
	    cache.get(id, (err, result) => {
		if (result !== undefined && result !== null) {
		    //if (id === JSON.parse(result)._id)
		    resolve(true);
		} else {
		    RoomsModel.find({ "_id": id }, function(error, comments) {
			if (comments !== undefined && comments.length > 0) {
			    cache.set(id, JSON.stringify(comments[0]));
			    console.log("mongo " + comments);
			    resolve(true);
			} else {
			    resolve(false);
			}
		    });
		}
	    });
	});
    },
    isUserAdminOfRoom(userID, roomID){
	return new Promise(function(resolve,reject){
	    cache.get(roomID, (err, result) => {
		if (result !== undefined && result !== null) {
		    var room = JSON.parse(result);
		    for (var i = 0; i <= room.administrators.length; i+=1){
			if (room.administrators[i] === userID)
			    resolve(true);
		    }
		    resolve(false);
		}
		else{
		    RoomsModel.find({ _id: roomID }, function(error, comments) {
                        if (comments !== undefined && comments.length > 0) {
			    cache.set(roomID, JSON.stringify(comments[0]));
			    for (var i = 0; i <= comments[0].administrators.length; i+=1){
				if (comments[0].administrators[i] === userID)
				    resolve(true);
			    }
			    resolve(false);
                        }
			else
			    resolve(false);
                    });
		}
	    });
	});
    },
    list(){
	return new Promise(function(resolve, reject){
	    RoomsModel.find({}, {"_id": 1,"title": 1, "description": 1, "createdBy": 1, "updatedBy": 1, "created": 1, "updated": 1, "isPasswordProtected": 1},function(error, comments){
		if (comments.length > 0) {
                    resolve(comments);
		} else {
                    reject(new Error("NO_ROOMS_FOUND"));
		}	
	    });
	});
    },
    generateToken(user, roomID, password) {
	return new Promise(function(resolve, reject) {
	    jwt.sign(
		{ roomID, username: user.username, userID: user.id },
		process.env.SECRET || "secret",
		{ expiresIn: process.env.TOKEN_EXPIRE || "1h" },
		function(err, token) {
		    if (token !== null && token !== undefined) {
			resolve(token);
		    } else {
			reject(new Error("TOKEN_FAILED"));
		    }
		}
	    );
	});
    },
    isPasswdExist(roomID){
	return new Promise(function(resolve, reject){
	    cache.get(roomID, function (err, value) {
		if (value !== undefined && value !== null) {
		    resolve(JSON.parse(value).isPasswordProtected)
		}
		else{
		    RoomsModel.find({ _id: roomID }, function(error, comments) {
			if (comments !== undefined && comments.length > 0) {
			    resolve(comments[0].isPasswordProtected);
			    cache.set(roomID, JSON.stringify(comments[0]));
			}
			else
			    resolve(false);
		    });
		}
	    });
	});
    },
    isPasswdAdminExist(roomID){
	return new Promise(function(resolve, reject){
	    cache.get(roomID, function (err, value) {
		if (value !== undefined && value !== null) {
		    resolve(Boolean(JSON.parse(value).passwordAdmin));
		}
		else{
		    RoomsModel.find({ _id: roomID }, function(error, comments) {
			if (comments !== undefined && comments.length > 0) {
			    resolve(Boolean(comments[0].passwordAdmin));
			    cache.set(roomID, JSON.stringify(comments[0]));
			}
			else
			    resolve(false);
		    });
		}
	    });
	});
    },
    setPassword({roomID, userID, password}){
	return new Promise(function(resolve, reject){
	    bcrypt.hash(password, 10, function (err, hash) {
	     cache.get(roomID, function (err, value) {
		if (value !== undefined && value !== null) {
		    var room = JSON.parse(value);
		    room.password = hash;
		    room.updated = Date.now();
		    room.updatedBy = userID;
		    cache.set(roomID, JSON.stringify(room));
		    resolve(true);
                }
	     });
		RoomsModel.updateMany({_id: roomID},
		    { $set: { password: hash, updated: Date.now(), updatedBy: userID } },
		    function(error, comments) {
			if (comments !== undefined) {
			    //resolve(comments);
			    //RoomsModel.find({ _id: id }, function(error, comments){
				//cache.set(id, JSON.stringify(comments[0]));
			    //})
			}
			else reject("ERROR");
		    })
	    });
	});
    },
    setAdminPassword({roomID, userID, password}){
	return new Promise(function(resolve, reject){
	    return new Promise(function(resolve, reject){
		bcrypt.hash(password, 10, function (err, hash) {
		    cache.get(roomID, function (err, value) {
			if (value !== undefined && value !== null) {
			    var room = JSON.parse(value);
			    room.passwordAdmin = hash;
			    room.updated = Date.now();
			    room.updatedBy = userID;
			    cache.set(roomID, JSON.stringify(room));
			    resolve(true);
			}
		    });
                    RoomsModel.updateMany({_id: roomID},
					  { $set: { passwordAdmin: hash, updated: Date.now(), updatedBy: userID } },
					  function(error, comments) {
					      if (comments !== undefined) {
					      }
					      else reject("ERROR");
					  });
		});
            });
	});
    },
    setIsPasswordProtected({roomID, userID, state}){
	return new Promise(function(resolve, reject){
	    cache.get(roomID, function (err, value) {
		if (value !== undefined && value !== null) {
		    var room = JSON.parse(value);
		    room.isPasswordProtected = state;
		    room.updated = Date.now();
		    room.updatedBy = userID;
		    cache.set(roomID, JSON.stringify(room));
		    resolve(state);
                }
	    });
	    RoomsModel.updateMany({ _id: roomID },
		{ $set: { isPasswordProtected: state, updated: Date.now(), updatedBy: userID } },
		function(error, comments) {
		    if (comments !== undefined) {
			//resolve(comments);
			//RoomsModel.find({ _id: id }, function(error, comments){
			//cache.set(id, JSON.stringify(comments[0]));
			//})
		    }
		    else {
			console.log(error);
			reject("ERROR")
		    };
		});
	    }); 
    },
    cmpPasswd(roomID, password, type){
	return new Promise(function(resolve, reject){
	    cache.get(roomID, function (err, value) {
                if (value !== undefined && value !== null) {
		    if (type === 'room')
			var passwd = JSON.parse(value).password;
		    else if (type === 'admin')
			var passwd = JSON.parse(value).passwordAdmin;
		    bcrypt
                    .compare(password, passwd)
                    .then(result => {
                        if (result === true) {
                            resolve(true);
                        }
                        else
                            resolve(false);
                    });
                }
                else{
                    RoomsModel.find({ _id: id }, function(error, comments) {
                        if (comments[0] !== undefined) {
                            cache.set(roomID, JSON.stringify(comments[0]));
			    if (type === 'room')
				var passwd = comments[0].password;
			    else if (type === 'admin')
				var passwd = comments[0].passwordAdmin;
			    bcrypt
				.compare(password, passwd)
				.then(result => {
				    if (result === true) {
					resolve(true);
				    }
				    else
					resolve(false);
				});
                        }
                    });
                }
	    });
	});
    },
    toggleUserPerm(roomID, userID){
	return new Promise(function(resolve, reject){
	    cache.get(roomID, function (err, value) {
                if (value !== undefined && value !== null) {
                    var room = JSON.parse(value);
		    if (room.administrators.indexOf(userID) > -1 && room.administrators.length > 1)
			room.administrators = room.administrators.filter(function(value, index, arr){ return value != userID;});
		    else
			room.administrators.push(userID);
		    resolve(room.administrators);
		    cache.set(roomID, JSON.stringify(room));
		}
	       });
	    RoomsModel.find({ _id: roomID }, function(error, comments) {
                if (comments !== undefined && comments[0].length > 0) {
		    if (comments[0].administrators.indexOf(userID) > -1 && room.administrators.length > 1)
		    {
			RoomsModel.updateMany(
                            { _id: roomID },
                            { $set: { updated: Date.now(), updatedBy: userID }, $pull: { administrators: userID } },
                            function(error, comments) {
			    });
		    }
		    else{
			RoomsModel.updateMany(
                            { _id: roomID },
                            { $set: { updated: Date.now(), updatedBy: userID }, $push: { administrators: userID } },
                            function(error, comments) {
                            });
		    }
		}
	    });
	});
    },
    edit(userID, id, title, description, password, isPasswordProtected, passwordAdmin) {
    return new Promise(function(resolve, reject) {
      RoomsModel.find({ _id: id }, function(error, comments) {
        if (comments[0] !== undefined) {
            title = title || comments[0].title;
            description = description || comments[0].description;

	    if (password !== undefined){
		bcrypt.hash(password, 10, function (err, hash) {
		    RoomsModel.updateMany(
			{ _id: id },
			{ $set: { password: hash, isPasswordProtected, updated: Date.now(), updatedBy: userID } },
			function(error, comments) {
			    if (comments !== undefined) {
				resolve(comments);
				RoomsModel.find({ _id: id }, function(error, comments){
				    cache.set(id, JSON.stringify(comments[0]));
				})
			    }
			    else reject("ERROR");
			}
		    );
		});
	    }
	    
	    if (passwordAdmin !== undefined){
		bcrypt.hash(passwordAdmin, 10, function (err, hash) {
		    RoomsModel.updateMany(
			{ _id: id },
			{ $set: { passwordAdmin: hash, updated: Date.now(), updatedBy: userID } },
			function(error, comments) {
			    if (comments !== undefined) {
				resolve(comments);
				RoomsModel.find({ _id: id }, function(error, comments){
				    cache.set(id, JSON.stringify(comments[0]));
				})
			    }
			    else reject("ERROR");
			}
		    );
		});
	    }
			
            RoomsModel.updateMany(
		{ _id: id },
		{ $set: { title, description, updated: Date.now(), updatedBy: userID } },
		function(error, comments) {
		    if (comments !== undefined) {
			resolve(comments);
			RoomsModel.find({ _id: id }, function(error, comments){
			    cache.set(id, JSON.stringify(comments[0]));
			})
		    }
		    else reject("ERROR");
		}
            );
        }
      });
    });
  }
};

module.exports = Room;
