// Import Node Modules
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// Import User Model
const UserModel = require("../models/User.js");
const RoomModel = require("../models/Room.js");

const cache = require("../db/cache.js").client;
const pubsub = require("../db/cache.js").subscriber;

const { promisify } = require("util");

const getAsync = promisify(cache.get).bind(cache);

module.exports = {
  isSocketIDConnected(clientID) {
    return new Promise(function (resolve, reject) {
      UserModel.find({ WebSocketID: clientID }, function (error, comments) {
        if (comments[0] !== undefined && comments[0] !== null) resolve(true);
        else resolve(false);
      });
    });
  },
    getUserFromClientID(WebSocketID) {
	return new Promise(function (resolve, reject) {
	    UserModel.find({ WebSocketID }, function (error, comments) {
		if (comments[0] !== null) resolve(comments[0]);
		else reject(new Error(false));
	    });
	});
    },
    addUserConnect(userID, roomID){
	return new Promise(function (resolve, reject){
	    cache.get(roomID, function (err, value) {
		if (value !== undefined && value !== null) {
                    var room = JSON.parse(value);
		    if (room.usersConnected === undefined)
			room.usersConnected = [userID];
                    else if (room.usersConnected.indexOf(userID) === -1)
			room.usersConnected.push(userID);
		    cache.set(roomID, JSON.stringify(room));
                    resolve(room.usersConnected);
		}
		else
		    console.log(err);
		RoomModel.find({"_id": roomID}, function(error, room){
		    if (room[0] !== undefined)
			if (room[0].usersConnected.indexOf(userID) === -1)
		    {
			RoomModel.updateMany(
			    { _id: roomID },
			    { $push: { usersConnected: userID }, $unset: { expireAt: 1 }  },
			    function (error, comments) {
				//console.log("CONNECTED in mongo");
				RoomModel.find({ "_id": roomID}, function(error, comments){
				    if (comments !== undefined){
					resolve(comments[0].usersConnected);
					cache.set(roomID, JSON.stringify(comments[0]));
				    }
				});
			    }
			);
		    }
		});
	    });
	});
    },
    deleteUserConnect(userID, roomID){
	return new Promise(function(resolve, reject){
            cache.get(roomID, function (err, value) {
                if (value !== undefined && value !== null) {
                    var room = JSON.parse(value);
                    room.connected = false;
                    room.usersConnected.splice(room.usersConnected.indexOf(userID), 1);
		    cache.set(roomID, JSON.stringify(room));
		    if (room.usersConnected.length === 0)
			cache.expire(roomID, 300);
		    resolve(room.usersConnected);
                }
                else
                    console.log(err);
                RoomModel.updateOne(
		    { _id: roomID },
		    { $pull: { usersConnected: userID } },
		    { multi: true },
		    function (error, comments) {
			//console.log(comments);
                        //console.log("DISCONNECTED on mongo");
			RoomModel.find({"_id": roomID}, function(error, comments){
			    if (comments.length > 0 && comments[0].usersConnected.length === 0){
				var date = new Date();
				RoomModel.updateOne({"_id": roomID}, { expireAt: date.setSeconds(date.getSeconds() + 300) }, function(err, data) {
				    
				});
			    }
			    //cache.set(roomID, JSON.stringify(comments[0]));
			});
		    }
	        );
            });
	});
    },
  connect(userID, clientID) {
      return new Promise(function (resolve, reject) {
	  cache.get(userID, function (err, value) {
              if (value !== undefined && value !== null) {
		  var user = JSON.parse(value);
		  user.connected = true;
		  if (user.WebSocketID === undefined)
		      user.WebSocketID = [clientID];
		  else
		      user.WebSocketID.push(clientID);
		  cache.set(userID, JSON.stringify(user));
		  resolve("CONNECTED");
	      }
	      else {
		  console.log(err);
	      }
	      UserModel.updateMany(
		  { _id: userID },
		  { $set: { connected: true }, $push: { WebSocketID: clientID }  },
		  function (error, comments) {
		      //console.log("CONNECTED in mongo");
		      UserModel.find({"_id": userID}, function(error, comments){
			  cache.set(userID, JSON.stringify(comments[0]));
		      });
		  }
	      );
	  });
      });
  },
  disconnect(clientID) {
      return new Promise(function (resolve, reject) {
	  UserModel.find({ WebSocketID: clientID }, function(error, comments){
	      if (comments[0] !== undefined){
		  cache.get(comments[0]._id.toString(), function (err, value) {
		      if (value !== undefined && value !== null) {
			  var user = JSON.parse(value);
			  user.connected = false;
			  user.WebSocketID = user.WebSocketID.filter(e => e !== clientID);
 			  cache.set(comments[0]._id.toString(), JSON.stringify(user));
			  resolve("DISCONNECTED");
		      }
		      else
			  console.log(err);
		      UserModel.updateMany(
		      { WebSocketID: clientID },
		      { $set: { connected: false }, $pull: { WebSocketID: clientID } },
		      function (error, comments) {
			  //console.log("DISCONNECTED on mongo");
		      }
		  );
		  });
	      }
	  });
      });
  }
};
