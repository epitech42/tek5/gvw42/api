// Import Node Modules
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// Import User Model
const UserModel = require("../models/User.js");

const cache = require("../db/cache.js").client;

const { promisify } = require("util");

const getAsync = promisify(cache.get).bind(cache);


module.exports = {
  async create(username, password) {
    return new Promise(function (resolve, reject) {
        //username = username + '#' + Random.num(5);
        bcrypt.hash(password, 10, function (err, hash) {
            const user = new UserModel({ username, password: hash });
	    cache.set(user._id.toString(), JSON.stringify(user));
	    cache.set(user.username, JSON.stringify(user));
            user.save().then(() => {
		//console.log("user save in DB");
		resolve({
		    message: "USER_CREATED",
		    username,
		    id: user._id 	
		});
            });
        });
    });
  },
  getUsernameFromID(id) {
    return new Promise(function (resolve, reject) {
      cache.get(id, function (err, value) {
        if (value !== undefined && value !== null) {
          resolve(JSON.parse(value).username);
        } else {
          UserModel.find({ _id: id }, function (error, comments) {
              if (comments === undefined || comments.length === 0) reject(new Error("USER_NOT_FOUND"));
            else {
              resolve(comments[0].username);
            }
          });
        }
      });
    }).catch(function (err) {
      throw new Error(err.message);
    });
  },
  delete(userID) {
    return new Promise(function (resolve, reject) {
	UserModel.find({ _id: userID }, function (error, comments) {
            if (comments === undefined) reject(new Error("USER_NOT_FOUND"));
            else {
		module.exports.getUsernameFromID(userID).then(function (username) {
		    cache.del(username, function (err, response) {
			//console.log("delete in redis" + response);
			cache.del(userID, function (err, response) {
			    //console.log("delete in redis " + response);
			    UserModel.deleteOne({ _id: userID }, function (error, comments) {
				if (comments !== undefined) {
				    resolve("USER_DELETED");
				}
				else
				    reject(new Error(error));
			    });
			});
		    });
		});
	    }
	})
    }).catch(function (err) {
      throw new Error(err.message);
    });
  },
  isUsernameExist(username) {
    return new Promise(function (resolve, reject) {
      // CHECKING CACHE
      cache.get(username, (err, result) => {
        if (result !== undefined && result !== null) {
          resolve(true);
        } else {
          // CACHING
            UserModel.find({ username: username }, function (error, comments) {
		if (comments[0] !== undefined) {
		    cache.set(username, JSON.stringify(comments[0]));
		    resolve(true);
		} else {
		    resolve(false);
		}
            });
        }
      });
    });
  },
  login(username) {
      return new Promise(function (resolve, reject) {
          cache.get(username, (err, result) => {
              if (result !== undefined && result !== null) {
		  var user = JSON.parse(result);
		  jwt.sign(
                      { id: user._id, username: user.username },
                      process.env.SECRET || "secret",
                      { expiresIn: process.env.TOKEN_EXPIRE || '24h' },
		      function (err, token) {
                          resolve({ username, token });
                      }
                  );
	      }
              else{
		  UserModel.find({ username }, function (error, comments) {
		      if (comments[0] !== undefined) {
			  jwt.sign(
			      { id: comments[0]._id, username: comments[0].username },
			      process.env.SECRET || "secret",
			      { expiresIn: process.env.TOKEN_EXPIRE || '24h' },
			      function (err, token) {
				  resolve({ username, token });
			      }
			  );
		      } else reject(new Error("USER_NOT_FOUND"));
		  }).catch(function (err) {
		      throw new Error(err.message);
		  });
	      }
	  });
      });
  },
    cmpPasswd(username, password){
	return new Promise(function (resolve, reject){
	    cache.get(username, (err, result) => {
		if (result !== undefined && result !== null) {
		    bcrypt
			.compare(password, JSON.parse(result).password)
			.then(result => { resolve(result) });
		}
		else {
		    UserModel.find({ username }, function (error, comments) {
			if (comments[0] !== undefined) {
			    bcrypt
				.compare(password, comments[0].password)
				.then(result => { resolve(result) });
			}
			else
			    reject(new Error("USERNAME_NOT_FOUND"));
		    });
		}
	    });
	});
    },
  verifyToken(token) {
    return new Promise(function (resolve, reject) {
      jwt.verify(token, process.env.SECRET || "secret", function (err, decoded) {
        if (decoded !== undefined) resolve(true);
        else resolve(false);
      });
    });
  },
  getUserfromToken(token) {
    return new Promise(function (resolve, reject) {
	jwt.verify(token, process.env.SECRET || "secret", function (err, decoded) {
            if (decoded !== undefined) resolve(decoded);
            else throw new Error("Token Error");
	});
    }).catch(function (err) {
	throw new Error(err.message);
    });
  },
  edit(id, username, password) {
      return new Promise(function (resolve, reject) {
	  cache.get(id.toString(), function (err, value) {
              if (value !== undefined && value !== null) {
                  var user = JSON.parse(value);
                  user.username = username || user.username;
		  if (password !== undefined)
		  {
		      bcrypt.hash(password, 10, function (err, hash) {
			  user.password = hash;
			  cache.set(id, JSON.stringify(user));
			  cache.set(user.username, JSON.stringify(user));
		      });
		  }
		  else{
		      cache.set(id, JSON.stringify(user));
		      cache.set(user.username, JSON.stringify(user));
		  }
		  resolve({ n: 1, nModified: 1, ok: 1 });
	      }
	      UserModel.find({ _id: id }, function (error, comments) {
		  //console.log(comments);
		  //console.log(error);
		  username = username || comments[0].username;
		  password = password || comments[0].password;
		  if (password !== comments[0].password){
		      bcrypt.hash(password, 10, function (err, hash) {
			  UserModel.updateMany(
			      { _id: id },
			      { $set: { username, password: hash, updated: Date.now() } },
			      function (error, comments) {
				  if (comments !== undefined) resolve(comments);
				  else reject(new Error("ERROR"));
			      }
			  );
		      })
		  }
		  else {
		      UserModel.updateMany(
			  { _id: id },
			  { $set: { username, updated: Date.now() } },
			  function (error, comments) {
			      if (comments !== undefined) resolve(comments);
			      else reject(new Error("ERROR"));
			  });
		  }
	      });
        /*comments[0].set({
            username,
            password,
            updated: Date.now()
        });
        comments[0].save();*/
        //resolve(true);
	  });
      });
  },
  search(string) {
    return new Promise(function (resolve, reject) {
      UserModel.find(
        { username: { $regex: string } },
        { _id: 1, username: 1 },
        function (error, users) {
          if (users !== undefined) resolve(users);
          else reject(new Error("NO_USER"));
        }
      );
    });
  },
  getUserFromID(id) {
      return new Promise(function (resolve, reject) {
	  cache.get(id, function (err, value) {
              if (value !== undefined && value !== null) {
		  var user = JSON.parse(value);
		  delete user.password;
		  delete user.__v;
		  resolve(user);
              } else {
		  UserModel.find({ _id: id }, { "password": 0, "__v": 0 }, function (error, comments) {
		      if (comments !== undefined && comments.length > 0)
			  resolve(comments[0]);
		      else
			  reject(new Error("USER_NOT_FOUND"));
		  });
	      }
	  });
      });
  }
};
