module.exports = {
  setTimeout(start) {
    // execution time simulated with setTimeout function
    const end = new Date() - start;

    return end;
  },

  display(start) {
    return `[${this.setTimeout(start)}ms] `;
  }
};
