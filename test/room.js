var assert = require('assert');
const request = require('supertest');

const db = require("../src/db/db.js");

const user01 = "user01"
const user02 = "user02"

const app = require('../src/index')

var ObjectID = new RegExp("^[0-9a-fA-F]{24}$");

describe('Room', function(){
    var token = "";
    var userID = "";
    var username = "";

    beforeEach(function(done) {
	username = Math.random().toString(36).substr(2, 5);
	// DELETE DB
	db.connection.db.dropDatabase();
	db.connection.collections['users'].drop(function(err){
	    //console.log('collection dropped');
	    request(app)
		.post('/user/register')
		.send({username: username, password: user01})
		.expect(function(res){
		    userID = res.body.id;
		    //console.log(res.body);
		})
		.end(function(err, res){
		    if (err) return resolve(err);
		    request(app)
			.post('/user/login')
			.send({username: username, password: user01})
			.expect(function(res){
			    token = res.body.token;
			})
			.end(function(err, res){
			    //if (err) return reject(err);
			    done();
			})
		}); 
	});
    });
    
    // Create User
    describe('Create Room', function() {
	it('Not exsting yet', function() {
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/room/create')
		    .set("token", token)
	            .send({title: user01, description: user01})
		    .expect(function(res) {
			if (ObjectID.test(res.body.roomID) === true)
			    res.body.roomID = "0";
		    })
	            .expect(201, {
			message: 'ROOM_CREATED',
			roomID: "0",
		    })
		    .end(function(err, res){
			if (err) return reject(err);
			resolve();
		    })
	    });
	});
	it('Good Token but User Delete', function() {
	    return new Promise(function(resolve, reject) {
		request(app)
		    .delete('/user/delete')
		    .set("token", token)
	            .end(function(err, res){
			if (err) return reject(err);
			request(app)
			    .post('/room/create')
			    .set("token", token)
			    .send({title: user01, description: user01})
			    .expect(422, {
				errors: [
                                    { location: "headers", msg: "USER_NOT_FOUND", param: "token", value: token }
				]
                            })
			    .end(function(err, res){
				if (err) return reject(err);
				resolve();
			    })
		    })
	    });
	});
	it('Wrong Token', function() {
	    return new Promise(function(resolve, reject) {
		request(app)
		    .delete('/user/delete')
		    .set("token", token)
	            .end(function(err, res){
			if (err) return reject(err);
			request(app)
			    .post('/room/create')
			    .set("token", "lol")
			    .send({title: user01, description: user01})
			    .expect(422, {
				errors: [
                                    { location: "headers", msg: "TOKEN_ERROR", param: "token", value: "lol" },
				    { location: "headers", msg: "Token Error", param: "token", value: "lol" }
				]
                            })
			    .end(function(err, res){
				if (err) return reject(err);
				resolve();
			    })
		    })
	    });
	});
    });

    describe('Room Login', function() {
	it('Exsting yet', function() {
	    var DateTest = new Date();
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/room/create')
		    .set("token", token)
	            .send({title: user01, description: user02})
		    .end(function(err, res){
			if (err) return reject(err);
			request(app)
			    .post('/room/login')
			    .set("token", token)
		            .send({id: res.body.roomID})
			    .expect(function(res) {
				res.body.token = "token";
			    })
			    .expect(200, {
				token: "token"
			    })
			    .end(function(err, res){
				if (err) return reject(err);
				resolve();
			    })
		    })
	    });
	});
	it('Not Exsting', function() {
	    var DateTest = new Date();
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/room/login')
		    .set("token", token)
		    .send({id: "fakeroom"})
		    .expect(422, {
			errors: [
			    { location: "body", msg: "ROOM_NOT_FOUND", param: "id", value: "fakeroom" }
			]
		    })
		    .end(function(err, res){
			if (err) return reject(err);
			resolve();
		    })
	    });
	});
    });

    describe('Edit List', function() {
	it('Exsting yet', function() {
	    var DateTest = new Date();
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/room/create')
		    .set("token", token)
	            .send({title: user01, description: user02})
		    .end(function(err, res){
			if (err) return reject(err);
			request(app)
			    .put('/room/edit')
			    .set("token", token)
			    .send({id: res.body.roomID, title: "ok", description: "ok"})
			    .expect(200, {
                                msg: { n: 1, nModified: 1, ok: 1 }
			    })
			    .end(function(err, res){
				if (err) return reject(err);
				resolve();
			    })
		    })
	    });
	});
	it('Not Existing', function() {
	    var DateTest = new Date();
	    return new Promise(function(resolve, reject) {
		request(app)
		    .put('/room/edit')
		    .set("token", token)
		    .send({id: "fakeRoomID", title: "ok", description: "ok"})
		    .expect(422, {
			errors: [
                            { location: "body", msg: "ROOM_NOT_FOUND", param: "id", value: "fakeRoomID"},
                            { location: "body", msg: "USER_NO_ADMIN_PERMISSIONS", param: "id", value: "fakeRoomID"}
                        ]
		    })
		    .end(function(err, res){
			if (err) return reject(err);
			resolve();
		    })
	    })
	});
	it('Exsting yet Wrong Token', function() {
	    var DateTest = new Date();
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/room/create')
		    .set("token", token)
	            .send({title: user01, description: user02})
		    .end(function(err, res){
			if (err) return reject(err);
			request(app)
			    .put('/room/edit')
			    .set("token", "lol")
			    .send({id: res.body.roomID, title: "ok", description: "ok"})
			    .expect(422, {
				errors: [
				    { location: "headers", msg: "TOKEN_ERROR", param: "token", value: "lol" },
				    { location: "headers", msg: "Token Error", param: "token", value: "lol" },
                                    { location: "body", msg: "Token Error", param: "id", value: res.body.roomID }
				]
			    })
			    .end(function(err, res){
				if (err) return reject(err);
				resolve();
			    })
		    })
	    });
	});
    });
    
    describe('Room List', function() {
	it('Exsting yet', function() {
	    var DateTest = new Date();
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/room/create')
		    .set("token", token)
	            .send({title: user01, description: user02})
		    .end(function(err, res){
			if (err) return reject(err);
			request(app)
			    .get('/room/list')
			    .set("token", token)
			    .expect(function(res) {
				if (Date.parse(res.body[0].created) !== "NaN")
                                    res.body[0].created = DateTest;
				if (Date.parse(res.body[0].updated) !== "NaN")
                                    res.body[0].updated = DateTest;
			    })
			    .expect(200, [
				{
				    "isPasswordProtected": false,
				    "_id": res.body.roomID,
				    "title": user01,
				    "description": user02,
				    "createdBy": userID,
				    "updatedBy": userID,
				    "created": DateTest,
				    "updated": DateTest
				}
			    ])
			    .end(function(err, res){
				if (err) return reject(err);
				resolve();
			    })
		    })
	    });
	});
	it('Not Existing', function() {
	    var DateTest = new Date();
	    return new Promise(function(resolve, reject) {
		request(app)
		    .get('/room/list')
		    .set("token", token)
		    .expect(400, {
			err: "NO_ROOMS_FOUND"
		    })
		    .end(function(err, res){
			if (err) return reject(err);
			resolve();
		    })
	    })
	});
	it('Exsting yet Wrong Token', function() {
	    var DateTest = new Date();
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/room/create')
		    .set("token", token)
	            .send({title: user01, description: user02})
		    .end(function(err, res){
			if (err) return reject(err);
			request(app)
			    .get('/room/list')
			    .set("token", "lol")
			    .expect(422, {
				errors: [
				    { location: "headers", msg: "TOKEN_ERROR", param: "token", value: "lol" },
                                    { location: "headers", msg: "Token Error", param: "token", value: "lol" }
				]
			    })
			    .end(function(err, res){
				if (err) return reject(err);
				resolve();
			    })
		    })
	    });
	});
    });
    
    describe('Delete Room', function() {
	it('Exsting yet', function() {
	    var DateTest = new Date();
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/room/create')
		    .set("token", token)
	            .send({title: user01, description: user02})
		    .end(function(err, res){
			if (err) return reject(err);
			request(app)
			    .delete('/room/delete')
			    .set("token", token)
		            .send({id: res.body.roomID})
			    .expect(200, {
				message: "ROOM_DELETED",
				roomID: res.body.roomID
			    })
			    .end(function(err, res){
				if (err) return reject(err);
				resolve();
			    })
		    })
	    });
	});
	it('Existing - Wrong Token', function() {
	    var DateTest = new Date();
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/room/create')
		    .set("token", token)
	            .send({title: user01, description: user02})
		    .end(function(err, res){
			if (err) return reject(err);
			request(app)
			    .delete('/room/delete')
			    .set("token", "wtf")
		            .send({id: res.body.roomID})
			    .expect(422, {
				errors: [
				    { location: "headers", msg: "TOKEN_ERROR", param: "token", value: "wtf" },
				    { location: "headers", msg: "Token Error", param: "token", value: "wtf" },
				    { location: "body", msg: "Token Error", param: "id", value: res.body.roomID }
				]
			    })
			    .end(function(err, res){
				if (err) return reject(err);
				resolve();
			    })
		    })
	    });
	});
	it('Not Existing - Wrong Token', function() {
	    var DateTest = new Date();
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/room/create')
		    .set("token", token)
	            .send({title: user01, description: user02})
		    .end(function(err, res){
			if (err) return reject(err);
			request(app)
			    .delete('/room/delete')
			    .set("token", token)
		            .send({id: "wtf"})
			    .expect(422, {
				errors: [
				    { location: "body", msg: "ROOM_NOT_FOUND", param: "id", value: "wtf"},
				    { location: "body", msg: "USER_NO_ADMIN_PERMISSIONS", param: "id", value: "wtf"}
				]
			    })
			    .end(function(err, res){
				if (err) return reject(err);
				resolve();
			    })
		    })
	    });
	});
	it('Not Existing', function() {
	    var DateTest = new Date();
	    return new Promise(function(resolve, reject) {
		request(app)
		    .delete('/room/delete')
		    .set("token", token)
		    .send({id: "wtf"})
		    .expect(422, {
			"errors": [
			    {
				"value": "wtf",
				"msg": "ROOM_NOT_FOUND",
				"param": "id",
				"location": "body"
			    },
			    {
				"value": "wtf",
				"msg": "USER_NO_ADMIN_PERMISSIONS",
				"param": "id",
				"location": "body"
			    }
			]
		    })
		    .end(function(err, res){
			if (err) return reject(err);
			resolve();
		    })
	    });
	});
    });
});
/*
  describe('Login User', function() {
  it('Exsting', function() {
  return new Promise(function(resolve, reject) {
  request(app)
  .post('/user/register')
  .send({username: user01, password: user01})
  .end(function(err, res){
  if (err) return resolve(err);
  request(app)
  .post('/user/login')
  .send({username: user01, password: user01})
  .expect(function(res){
  res.body.token = "token";
  })
  .expect(200, {
  username: user01,
  token: "token"
  })
  .end(function(err, res){
  if (err) return reject(err);
  resolve();
  })
  }); 
  });
  });
  it('Not Exsting', function() {
  return new Promise(function(resolve, reject) {
  request(app)
  .post('/user/login')
  .send({username: user02, password: user02})
  .expect(422, {
  errors: [
  {
  "value": user02,
  "msg": "USERNAME_NOT_FOUND",
  "param": "username",
  "location": "body"
  },
  {
  "value": user02,
  "msg": "USERNAME_NOT_FOUND",
  "param": "password",
  "location": "body"
  }
  ]
  })
  .end(function(err, res){
  if (err) return reject(err);
  resolve()
  })
  });
  });
  it('No Username Submit', function() {
  return new Promise(function(resolve, reject) {
  request(app)
  .post('/user/login')
  .send({password: user01})
  .expect(422, {
  errors: [
  {
  "msg": "Invalid value",
  "param": "username",
  "location": "body"
  },
  {
  "msg": "USERNAME_NOT_FOUND",
  "param": "username",
  "location": "body"
  },
  {
  "value": user01,
  "msg": "USERNAME_NOT_FOUND",
  "param": "password",
  "location": "body"
  }
  ]
  })
  .end(function(err, res){
  if (err) return reject(err);
  resolve()
  })
  });
  });
  it('Wrong Password', function() {
  return new Promise(function(resolve, reject) {
  request(app)
  .post('/user/register')
  .send({username: user01, password: user01})
  .expect(201, {
  message: 'USER_CREATED',
  username: user01
  })
  .end(function(err, res){
  if (err) return resolve(err);
  request(app)
  .post('/user/login')
  .send({username: user01, password: user02})
  .expect(422, {
  errors: [
  {
  "value": user01,
  "msg": "WRONG_PASSWORD",
  "param": "password",
  "location": "body"
  }
  ]
  })
  .end(function(err, res){
  if (err) return reject(err);
  resolve();
  });
  });
  });
  });
  });

  describe('Get User', function() {
  it('Exsting', function() {
  return new Promise(function(resolve, reject) {
  const DateTest = new Date();
  request(app)
  .post('/user/register')
  .send({username: user01, password: user01})
  .end(function(err, res){
  if (err) return resolve(err);
  request(app)
  .post('/user/login')
  .send({username: user01, password: user01})
  .end(function(err, res){
  request(app)
  .get('/user/get')
  .set('token', JSON.parse(res.text).token)
  .expect(function(res) {
  if (ObjectID.test(res.body._id) === true)
  res.body._id = "0";
  if (Date.parse(res.body.created) !== "NaN")
  res.body.created = DateTest;
  if (Date.parse(res.body.updated) !== "NaN")
  res.body.updated = DateTest;
  })
  .expect(200, {
  username: user01,
  _id: "0",
  WebSocketID: [],
  connected: false,
  created: DateTest,
  updated: DateTest
  })
  .end(function(err, res){
  if (err) return reject(err);
  resolve();
  })
  })
  }); 
  });
  });
  it('No token', function() {
  return new Promise(function(resolve) {
  request(app)
  .get('/user/get')
  x		.expect(422, {
		    errors: [
			{
			    "msg": "Invalid value",
			    "param": "token",
			    "location": "body"
			},
			{
			    "msg": "TOKEN_ERROR",
			    "param": "token",
			    "location": "body"
			},
			{
			    "msg": "Token Error",
			    "param": "token",
			    "location": "body"
			}
		    ]
		})
		.end(function(err, res){
		    if (err) return resolve(err);
		    resolve()
		})
	})
    });
    it('Wrong token', function() {
	return new Promise(function(resolve) {
	    request(app)
		.get('/user/get')
		.expect(422, {
		    errors: [
			{
			    "msg": "TOKEN_ERROR",
			    "param": "token",
			    "location": "body"
			},
			{
			    "msg": "Token Error",
			    "param": "token",
			    "location": "body"
			}
		    ]
		})
		.end(function(err, res){
		    if (err) return resolve(err);
		    resolve()
		})
	})
    });
});

describe('Edit User', function() {
    it('Existing', function() {
	return new Promise(function(resolve, reject) {
	    request(app)
		.post('/user/register')
	        .send({username: user01, password: user01})
	        .end(function(err, res){
		    if (err) return resolve(err);
		    request(app)
			.post('/user/login')
			.send({username: user01, password: user01})
		        .end(function(err, res){
			    request(app)
				.put('/user/edit')
			        .set('Content-Type', 'application/json')
			        .set('token', JSON.parse(res.text).token)
			        .send({username: user01, password: user01})
				.expect(200, {
				    "msg": {
					"n": 1,
					"nModified": 1,
					"ok": 1
				    }
				})
				.end(function(err, res){
				    if (err) return reject(err);
				    resolve();
				})
			})
                }); 
	});
    });
    it('No Token', function() {
	return new Promise(function(resolve, reject) {
	    request(app)
		.put('/user/edit')
		.expect(422, {
		    "errors": [
			{ msg: "Invalid value", param: "token", location: "body" },
			{ msg: "TOKEN_ERROR", param: "token", location: "body" },
			{ msg: "Token Error", param: "token", location: "body"},
			{ msg: 'Token Error', param: 'username', location: 'body' }
		    ]
		})
		.end(function(err, res){
		    if (err) return reject(err);
		    resolve();
		})
	});
    });
    it('Wrong Token', function() {
	return new Promise(function(resolve, reject) {
	    request(app)
		.put('/user/edit')
	        .set('token', "xD")
		.expect(422, {
		    "errors": [
			{ value: "xD", msg: "TOKEN_ERROR", param: "token", location: "headers"},
			{ value: "xD", msg: "Token Error", param: "token", location: "headers"},
			{ msg: 'Token Error', param: 'username', location: 'body' }

		    ]
		})
		.end(function(err, res){
		    if (err) return reject(err);
		    resolve();
		})
	});
    }); 
});


describe('Delete User', function() {
    it('Existing', function() {
	return new Promise(function(resolve, reject) {
	    request(app)
		.post('/user/register')
	        .send({username: user01, password: user01})
	        .end(function(err, res){
		    if (err) return resolve(err);
		    request(app)
			.post('/user/login')
			.send({username: user01, password: user01})
		        .end(function(err, res){
			    request(app)
				.delete('/user/delete')
			        .set('Content-Type', 'application/json')
			        .set('token', JSON.parse(res.text).token)
				.expect(200, {
				    message: "USER_DELETED",
				    username: user01
				})
				.end(function(err, res){
				    if (err) return reject(err);
				    resolve();
				})
			})
                }); 
	});
    });
    it('No Token', function() {
	return new Promise(function(resolve, reject) {
	    request(app)
		.delete('/user/delete')
		.set('Content-Type', 'application/json')
		.expect(422, {
		    errors: [
			{ msg: "Invalid value", param: "token", location: "body" },
                        { msg: "TOKEN_ERROR", param: "token", location: "body" },
                        { msg: "Token Error", param: "token", location: "body"},
		    ]
		})
		.end(function(err, res){
		    if (err) return reject(err);
		    resolve();
		})
	});
    });
    it('Wrong Token', function() {
	return new Promise(function(resolve, reject) {
	    request(app)
		.delete('/user/delete')
		.set('Content-Type', 'application/json')
	        .set('token', 'token')
		.expect(422, {
		    errors: [
                        { msg: "TOKEN_ERROR", param: "token", location: "headers", value: "token"},
                        { msg: "Token Error", param: "token", location: "headers", value: "token"},
		    ]
		})
		.end(function(err, res){
		    if (err) return reject(err);
		    resolve();
		})
	});
    });
});
*/
