var assert = require('assert');
const request = require('supertest');

const db = require("../src/db/db.js");

const user01 = "user01"
const user02 = "user02"

const app = require('../src/index')

describe('User', function() {

    beforeEach(function(done) {
	// DELETE DB
	db.connection.db.dropDatabase();
	db.connection.collections['users'].drop(function(err){
	    //console.log('collection dropped');
	    done();
	});
    });
    
    var ObjectID = new RegExp("^[0-9a-fA-F]{24}$");

    // Create User
    describe('Create User', function() {
	it('Not exsting yet', function() {
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/user/register')
	            .send({username: user01, password: user01})
		    .expect(function(res) {
			if (ObjectID.test(res.body.id) === true)
			    res.body.id = "0";
		    })
	            .expect(201, {
			message: 'USER_CREATED',
			username: user01,
			id: "0",
		    })
		    .end(function(err, res){
			//console.log("err" + JSON.stringify(err));
			//console.log("res" + JSON.stringify(res));
			if (err) return reject(err);
			resolve();
		    })
	    });
	});
	it('Exsting yet', function() {
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/user/register')
	            .send({username: user01, password: user01})
	            .end(function(err, res){
			if (err) return resolve(err);
			request(app)
			    .post('/user/register')
			    .send({username: user01, password: user01})
			    .expect(422, {
				errors: [{
				    value: user01,
				    msg: "USERNAME_ALREADY_EXISTING",
				    param: "username",
				    location: "body"
				}]
                            })
			    .end(function(err, res){
				if (err) return reject(err);
				resolve()
			    })
		    })
	    });
	});
	it('Bad Syntax', function() {
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/user/register')
	            .send({username: user01 + "?", password: user01})
	            .expect(422, {
			errors: [
			    { location: "body", msg: "USERNAME_NOT_VALID", param: "username", value: user01 + "?"}
			]
		    })
		    .end(function(err, res){
			if (err) return reject(err);
			resolve();
		    })
	    });
	});
    });

    describe('Login User', function() {
	it('Exsting', function() {
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/user/register')
	            .send({username: user01, password: user01})
	            .end(function(err, res){
			if (err) return resolve(err);
			request(app)
			    .post('/user/login')
			    .send({username: user01, password: user01})
		            .expect(function(res){
				res.body.token = "token";
			    })
			    .expect(200, {
				username: user01,
				token: "token"
			    })
			    .end(function(err, res){
				if (err) return reject(err);
				resolve();
			    })
                    }); 
	    });
	});
	it('Not Exsting', function() {
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/user/login')
		    .send({username: user02, password: user02})
		    .expect(422, {
			errors: [
			    {
				"value": user02,
				"msg": "USERNAME_NOT_FOUND",
				"param": "username",
				"location": "body"
			    },
			    {
				"value": user02,
				"msg": "USERNAME_NOT_FOUND",
				"param": "password",
				"location": "body"
			    }
			]
		    })
		    .end(function(err, res){
			if (err) return reject(err);
			resolve()
		    })
	    });
	});
	it('No Username Submit', function() {
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/user/login')
		    .send({password: user01})
		    .expect(422, {
			errors: [
			    {
				"msg": "Invalid value",
				"param": "username",
				"location": "body"
			    },
			    {
				"msg": "USERNAME_NOT_FOUND",
				"param": "username",
				"location": "body"
			    },
			    {
				"value": user01,
				"msg": "USERNAME_NOT_FOUND",
				"param": "password",
				"location": "body"
			    }
			]
		    })
		    .end(function(err, res){
			if (err) return reject(err);
			resolve()
		    })
	    });
	});
	it('Wrong Password', function() {
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/user/register')
	            .send({username: user01, password: user01})
	            .expect(201, {
			message: 'USER_CREATED',
			username: user01
		    })
	            .end(function(err, res){
			if (err) return resolve(err);
			request(app)
			    .post('/user/login')
			    .send({username: user01, password: user02})
			    .expect(422, {
				errors: [
				    {
					"value": user01,
					"msg": "WRONG_PASSWORD",
					"param": "password",
					"location": "body"
				    }
				]
			    })
			    .end(function(err, res){
				if (err) return reject(err);
				resolve();
			    });
                    });
	    });
	});
    });

    describe('Get User', function() {
	it('Exsting', function() {
	    return new Promise(function(resolve, reject) {
		const DateTest = new Date();
		request(app)
		    .post('/user/register')
	            .send({username: user01, password: user01})
	            .end(function(err, res){
			if (err) return resolve(err);
			request(app)
			    .post('/user/login')
			    .send({username: user01, password: user01})
		            .end(function(err, res){
				request(app)
				    .get('/user/get')
			            .set('token', JSON.parse(res.text).token)
				    .expect(function(res) {
					if (ObjectID.test(res.body._id) === true)
					    res.body._id = "0";
					if (Date.parse(res.body.created) !== "NaN")
					    res.body.created = DateTest;
					if (Date.parse(res.body.updated) !== "NaN")
					    res.body.updated = DateTest;
				    })
				    .expect(200, {
					username: user01,
					_id: "0",
					WebSocketID: [],
					connected: false,
					created: DateTest,
					updated: DateTest
				    })
				    .end(function(err, res){
					if (err) return reject(err);
					resolve();
				    })
			    })
                    }); 
	    });
	});
	it('No token', function() {
	    return new Promise(function(resolve) {
		request(app)
		    .get('/user/get')
		    .expect(422, {
			errors: [
			    {
				"msg": "Invalid value",
				"param": "token",
				"location": "body"
			    },
			    {
				"msg": "TOKEN_ERROR",
				"param": "token",
				"location": "body"
			    },
			    {
				"msg": "Token Error",
				"param": "token",
				"location": "body"
			    }
			]
		    })
		    .end(function(err, res){
			if (err) return resolve(err);
			resolve()
		    })
	    })
	});
	it('Wrong token', function() {
	    return new Promise(function(resolve) {
		request(app)
		    .get('/user/get')
		    .expect(422, {
			errors: [
			    {
				"msg": "TOKEN_ERROR",
				"param": "token",
				"location": "body"
			    },
			    {
				"msg": "Token Error",
				"param": "token",
				"location": "body"
			    }
			]
		    })
		    .end(function(err, res){
			if (err) return resolve(err);
			resolve()
		    })
	    })
	});
	it('Good Token but User Delete', function() {
	    return new Promise(function(resolve, reject) {
		const DateTest = new Date();
		request(app)
		    .post('/user/register')
	            .send({username: user01, password: user01})
	            .end(function(err, res){
			if (err) return resolve(err);
			request(app)
			    .post('/user/login')
			    .send({username: user01, password: user01})
		            .end(function(err, res){
				request(app)
				    .delete('/user/delete')
				    .set("token", JSON.parse(res.text).token)
				    .end(function(err){
					if (err) return reject(err);
					request(app)
					    .get('/user/get')
					    .set('token', JSON.parse(res.text).token)
					    .expect(422, {
						errors: [
						    { location: "headers", msg: "USER_NOT_FOUND", param: "token", value: JSON.parse(res.text).token }
						]
					    })
					    .end(function(err, res){
						if (err) return reject(err);
						resolve();
					    })
				    })
			    });
                    }); 
	    });
	});
    });

    describe('Edit User', function() {
	it('Existing', function() {
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/user/register')
	            .send({username: user01, password: user01})
	            .end(function(err, res){
			if (err) return resolve(err);
			request(app)
			    .post('/user/login')
			    .send({username: user01, password: user01})
		            .end(function(err, res){
				request(app)
				    .put('/user/edit')
			            .set('Content-Type', 'application/json')
			            .set('token', JSON.parse(res.text).token)
			            .send({username: user01, password: user01})
				    .expect(200, {
					"msg": {
					    "n": 1,
					    "nModified": 1,
					    "ok": 1
					}
				    })
				    .end(function(err, res){
					if (err) return reject(err);
					resolve();
				    })
			    })
                    }); 
	    });
	});
	it('No Token', function() {
	    return new Promise(function(resolve, reject) {
		request(app)
		    .put('/user/edit')
		    .expect(422, {
			"errors": [
			    { msg: "Invalid value", param: "token", location: "body" },
			    { msg: "TOKEN_ERROR", param: "token", location: "body" },
			    { msg: "Token Error", param: "token", location: "body"},
			    { msg: 'Token Error', param: 'username', location: 'body' }
			]
		    })
		    .end(function(err, res){
			if (err) return reject(err);
			resolve();
		    })
	    });
	});
	it('Wrong Token', function() {
	    return new Promise(function(resolve, reject) {
		request(app)
		    .put('/user/edit')
	            .set('token', "xD")
		    .expect(422, {
			"errors": [
			    { value: "xD", msg: "TOKEN_ERROR", param: "token", location: "headers"},
			    { value: "xD", msg: "Token Error", param: "token", location: "headers"},
			    { msg: 'Token Error', param: 'username', location: 'body' }

			]
		    })
		    .end(function(err, res){
			if (err) return reject(err);
			resolve();
		    })
	    });
	});
	it('Bad Syntax', function() {
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/user/register')
	            .send({username: user01, password: user01})
	            .end(function(err, res){
			if (err) return resolve(err);
			request(app)
			    .post('/user/login')
			    .send({username: user01, password: user01})
		            .end(function(err, res){
				request(app)
				    .put('/user/edit')
			            .set('Content-Type', 'application/json')
			            .set('token', JSON.parse(res.text).token)
			            .send({username: user01+"?", password: user01})
				    .expect(422, {
					errors: [
					    { location: "body", msg: "USERNAME_NOT_VALID", param: "username", value: user01 + "?"}
					]
				    })
				    .end(function(err, res){
					if (err) return reject(err);
					resolve();
				    });
			    });
                    });
	    });
	});
    });


    describe('Delete User', function() {
	it('Existing', function() {
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/user/register')
	            .send({username: user01, password: user01})
	            .end(function(err, res){
			if (err) return resolve(err);
			request(app)
			    .post('/user/login')
			    .send({username: user01, password: user01})
		            .end(function(err, res){
				request(app)
				    .delete('/user/delete')
			            .set('Content-Type', 'application/json')
			            .set('token', JSON.parse(res.text).token)
				    .expect(200, {
					message: "USER_DELETED",
					username: user01
				    })
				    .end(function(err, res){
					if (err) return reject(err);
					resolve();
				    })
			    })
                    }); 
	    });
	});
	it('No Token', function() {
	    return new Promise(function(resolve, reject) {
		request(app)
		    .delete('/user/delete')
		    .set('Content-Type', 'application/json')
		    .expect(422, {
			errors: [
			    { msg: "Invalid value", param: "token", location: "body" },
                            { msg: "TOKEN_ERROR", param: "token", location: "body" },
                            { msg: "Token Error", param: "token", location: "body"},
			]
		    })
		    .end(function(err, res){
			if (err) return reject(err);
			resolve();
		    })
	    });
	});
	it('Wrong Token', function() {
	    return new Promise(function(resolve, reject) {
		request(app)
		    .delete('/user/delete')
		    .set('Content-Type', 'application/json')
	            .set('token', 'token')
		    .expect(422, {
			errors: [
                            { msg: "TOKEN_ERROR", param: "token", location: "headers", value: "token"},
                            { msg: "Token Error", param: "token", location: "headers", value: "token"},
			]
		    })
		    .end(function(err, res){
			if (err) return reject(err);
			resolve();
		    })
	    });
	});
        it('Good Token but User Delete', function() {
	    return new Promise(function(resolve, reject) {
		request(app)
		    .post('/user/register')
	            .send({username: user01, password: user01})
	            .end(function(err, res){
			if (err) return resolve(err);
			request(app)
			    .post('/user/login')
			    .send({username: user01, password: user01})
		            .end(function(err, res){
				request(app)
				    .delete('/user/delete')
			            .set('Content-Type', 'application/json')
			            .set('token', JSON.parse(res.text).token)
				    .expect(200, {
					message: "USER_DELETED",
					username: user01
				    })
				    .end(function(err, response){
					if (err) return reject(err);
					request(app)
					    .delete('/user/delete')
					    .set('Content-Type', 'application/json')
					    .set('token', JSON.parse(res.text).token)
					    .expect(422, {
						errors: [
						    { location: "headers", msg: "USER_NOT_FOUND", param: "token", value: JSON.parse(res.text).token}
						]
					    })
					    .end(function(err, res){
						if (err) return reject(err);
						resolve();
					    })
				    })
			    })
                    }); 
	    });
	});
    });
});
