var assert = require('assert');
var io = require('socket.io-client');

const request = require('supertest');

const db = require("../src/db/db.js");

const user01 = "user01"
const user02 = "user02"

const app = require('../src/index')

// BEGIN SOCKET EVENT
const roomLoginEvent = "room:login";

const videoAddEvent = "video:add";
const videoDeleteEvent = "video:delete";

const videoAutoPlayEvent = "video:autoplay";
const videoPlayStatusEvent = "video:playStatus";
const videoOnSeekEvent = "video:seek";
const videoPlayerTimeEvent = "video:playerTime";

const videoMoveUpEvent = "video:moveUp";
const videoMoveDownEvent = "video:moveDown";


const usersGetInfosEvent = "users:getInfos";
const usersConnectedEvent = "users:count";

const messageEvent = "message";



const roomSetPasswordEvent = "room:setPassword";
const roomSetAdminPasswordEvent = "room:setAdminPassword";
const roomSetIsPasswordProtectedEvent = "room:setIsPasswordProtected";

const roomAskAuthEvent = "room:askauth";

const roomToggleUserPermEvent = "room:toggleUserPerm";

const success = ":success";
const err = ":err";
//END SOCKET EVENT

var userID = "";

var username = "";
var password = "";

var title = "";
var description = "";

var roomID = "";
var roomToken = "";
var socket = "";

var socket2 = "";
var roomToken2 = "";

var ioOptions = { 
    transports: ['websocket'],
    forceNew: true,
    reconnection: true
}

describe('Socket', function() {
    
    beforeEach(function(done) {
	// DELETE DB
	db.connection.db.dropDatabase();
	db.connection.collections['users'].drop(function(err){
	    db.connection.collections['rooms'].drop(function(err){
		//console.log('collection dropped');
		username = Math.random().toString(36).substr(2, 5);
		password = Math.random().toString(36).substr(2, 5);
		title = Math.random().toString(36).substr(2, 5);
		description = Math.random().toString(36).substr(2, 5);
		
		request(app)
                    .post('/user/register')
                    .send({username: username, password: user01})
                    .expect(function(res){
			userID = res.body.id;
			//console.log(res.body);
                    })
                    .end(function(err, res){
			if (err) return resolve(err);
			request(app)
                            .post('/user/login')
                            .send({username: username, password: user01})
                            .expect(function(res){
				token = res.body.token;
                            })
                            .end(function(err, res){
				//if (err) return reject(err);
				request(app)
				    .post('/room/create')
				    .set("token", token)
				    .send({title: title, description: description})
				    .expect(function(res) {
					roomID = res.body.roomID;
				    })
				    .end(function(err, res){
					if (err) return reject(err);
					request(app)
					    .post('/room/login')
					    .set("token", token)
					    .send({id: res.body.roomID})
					    .expect(function(res){
						roomToken = res.body.token;
					    })
					    .end(function(err, res){
						if (err) return reject(err);
						socket = io('http://127.0.0.1:1337/room-' + roomID, ioOptions);
						done();
					    })
				    })
                            })
                    });
	    });
	});
    });

    afterEach(function(done){
	socket.disconnect();
	done();
    });
    
    var ObjectID = new RegExp("^[0-9a-fA-F]{24}$");

    describe('Login', function() {
	it('Not exsting yet', function() {
	    return new Promise(function(resolve, reject) {
		socket.emit(roomLoginEvent, roomToken);
		socket.off(roomLoginEvent + success).on(roomLoginEvent + success, function(data){
		    //console.log("success" + JSON.stringify(data));
		    assert(data.message, "CONNECTED");
		    //socket.emit(roomLoadEvent, "");
		    resolve();
		});

		socket.off(roomLoginEvent + err).on(roomLoginEvent + err, function(data){
		    console.log('err login ' + data);
		    reject();
		});
	    });
	});
    });
    
    describe('Get Info User', function() {
	it('1 user', function() {
	    return new Promise(function(resolve, reject) {
		socket.emit(roomLoginEvent, roomToken);
		socket.off(roomLoginEvent + success).on(roomLoginEvent + success, function(data){
		    socket.emit(usersGetInfosEvent, [userID]);
		    socket.off(usersGetInfosEvent + success).on(usersGetInfosEvent + success, function(data){
			assert(data[0].username, username);
			resolve();
		    });
		    socket.off(usersGetInfosEvent + err).on(usersGetInfosEvent + err, function(data){
			console.log('err login ' + data);
			reject();
		    });
		});
	    });
	});
    });
    
    describe('Video AutoPlay', function() {
	it('True', function() {
	    return new Promise(function(resolve, reject) {
		socket.emit(roomLoginEvent, roomToken);
		socket.off(roomLoginEvent + success).on(roomLoginEvent + success, function(data){
		    socket.emit(videoAutoPlayEvent, true);
		    socket.off(videoAutoPlayEvent + success).on(videoAutoPlayEvent + success, function(autoPlay){
			assert(autoPlay, true);
			resolve();
		    });
		    socket.off(videoAutoPlayEvent + err).on(videoAutoPlayEvent + err, function(err){
			console.log(err);
			reject();
		    });
		});
	    });
	});
	/*it('False', function() {
	    return new Promise(function(resolve, reject) {
		socket.emit(roomLoginEvent, roomToken);
		socket.off(roomLoginEvent + success).on(roomLoginEvent + success, function(data){
		    socket.emit(videoAutoPlayEvent, false);
		    socket.off(videoAutoPlayEvent + success).on(videoAutoPlayEvent + success, function(autoPlay){
			assert(autoPlay, false);
			resolve();
		    });
		    socket.off(videoAutoPlayEvent + err).on(videoAutoPlayEvent + err, function(err){
			console.log(err);
			reject();
		    });
		});
	    });
	});*/
    });
    
    describe('Video PlayerState', function() {
	it('True', function() {
	    return new Promise(function(resolve, reject) {
		socket.emit(roomLoginEvent, roomToken);
		socket.off(roomLoginEvent + success).on(roomLoginEvent + success, function(data){
		    socket.emit(videoPlayStatusEvent, true);
		    socket.off(videoPlayStatusEvent + success).on(videoPlayStatusEvent + success, function(play){
			assert(play, true);
			resolve();
		    });
		    socket.off(videoPlayStatusEvent + err).on(videoPlayStatusEvent + err, function(err){
			console.log(err);
			reject();
		    });
		});
	    });
	});
    });

    describe('Video setPlayerTime', function() {
	it('True', function() {
	    return new Promise(function(resolve, reject) {
		socket.emit(roomLoginEvent, roomToken);
		socket.off(roomLoginEvent + success).on(roomLoginEvent + success, function(data){
		    socket.emit(videoPlayerTimeEvent, 0.5);
		    socket.off(videoPlayerTimeEvent + success).on(videoPlayerTimeEvent + success, function(time){
			assert(time, 0.5);
			resolve();
		    });
		    socket.off(videoPlayerTimeEvent + err).on(videoPlayerTimeEvent + err, function(err){
			console.log(err);
			reject();
		    });
		});
	    });
	});
    });
    
    describe('Message', function() {
	it('Send a MSG', function() {
	    return new Promise(function(resolve, reject) {
		socket.emit(roomLoginEvent, roomToken);
		socket.off(roomLoginEvent + success).on(roomLoginEvent + success, function(data){
		    socket.emit(messageEvent, "bjr");
		    socket.on(messageEvent + success, function(play){
			socket.off(messageEvent + success);
			assert(play.message, "bjr");
			resolve();
		    });
		    socket.off(videoPlayStatusEvent + err).on(videoPlayStatusEvent + err, function(err){
			console.log(err);
			reject();
		    });
		});
	    });
	});
    });

    describe('Set Room Password', function() {
	it('Existing Room', function() {
	    return new Promise(function(resolve, reject) {
		socket.emit(roomLoginEvent, roomToken);
		socket.off(roomLoginEvent + success).on(roomLoginEvent + success, function(data){
		    socket.emit(roomSetPasswordEvent, "test");
		    socket.off(roomSetPasswordEvent + success).on(roomSetPasswordEvent + success, function(value){
			resolve();
		    });
		    socket.off(videoOnSeekEvent + err).on(videoOnSeekEvent + err, function(err){
			console.log(err);
			reject();
		    });
		});
	    });
	});
    });
    
    describe('Set Room Admin Password', function() {
	it('Existing Room', function() {
	    return new Promise(function(resolve, reject) {
		socket.emit(roomLoginEvent, roomToken);
		socket.off(roomLoginEvent + success).on(roomLoginEvent + success, function(data){
		    socket.emit(roomSetPasswordEvent, "test");
		    socket.off(roomSetPasswordEvent + success).on(roomSetPasswordEvent + success, function(value){
			resolve();
		    });
		    socket.off(videoOnSeekEvent + err).on(videoOnSeekEvent + err, function(err){
			console.log(err);
			reject();
		    });
		});
	    });
	});
    });
    
    describe('Set Room is Password Protected', function() {
	it('Existing Room', function() {
	    return new Promise(function(resolve, reject) {
		socket.emit(roomLoginEvent, roomToken);
		socket.off(roomLoginEvent + success).on(roomLoginEvent + success, function(data){
		    socket.emit(roomSetIsPasswordProtectedEvent, true);
		    socket.off(roomSetIsPasswordProtectedEvent + success).on(roomSetIsPasswordProtectedEvent + success, function(value){
			assert(value, true);
			resolve();
		    });
		    socket.off(roomSetIsPasswordProtectedEvent + err).on(roomSetIsPasswordProtectedEvent + err, function(err){
			console.log(err);
			reject();
		    });
		});
	    });
	});
    });

    describe('Toggle User Perm', function() {
        it('Existing Room', function() {
            return new Promise(function(resolve, reject) {
                socket.emit(roomLoginEvent, roomToken);
                socket.off(roomLoginEvent + success).on(roomLoginEvent + success, function(data){
                    socket.emit(roomToggleUserPermEvent, userID);
		    socket.off(roomToggleUserPermEvent + success).on(roomToggleUserPermEvent + success, function(value){
                        //assert(value, true);
	                resolve();
                    });
                    socket.off(roomToggleUserPermEvent + err).on(roomToggleUserPermEvent + err, function(err){
                        console.log(err);
                        reject();
                    });
                });
            });
	});
    });

    
    describe('Video Seek', function() {
	it('0.98', function() {
	    return new Promise(function(resolve, reject) {
		socket.emit(roomLoginEvent, roomToken);
		socket.off(roomLoginEvent + success).on(roomLoginEvent + success, function(data){
		    socket.emit(videoOnSeekEvent, 0.98);
		    socket.off(videoOnSeekEvent + success).on(videoOnSeekEvent + success, function(time){
			assert(time, 0.98);
			resolve();
		    });
		    socket.off(videoOnSeekEvent + err).on(videoOnSeekEvent + err, function(err){
			console.log(err);
			reject();
		    });
		});
	    });
	});
    });
    
    describe('Video Add', function() {
	it('Not exsting yet', function() {
	    return new Promise(function(resolve, reject) {
		socket.emit(roomLoginEvent, roomToken);
		socket.off(roomLoginEvent + success).on(roomLoginEvent + success, function(data){
		    socket.emit(videoAddEvent, "https://youtube.com/tamere");
		    socket.off(videoAddEvent + success).on(videoAddEvent + success, function(video){
			assert(video.url, "https;//youtube.com/tamere");
			resolve();
		    });
		    socket.off(videoAddEvent + err).on(videoAddEvent + err, function(err){
			console.log(err);
			reject();
		    });
		});
	    });
	});
    });

    describe('Video Delete', function() {
	it('Existing', function() {
	    return new Promise(function(resolve, reject) {
		socket.emit(roomLoginEvent, roomToken);
		socket.off(roomLoginEvent + success).on(roomLoginEvent + success, function(data){
		    socket.emit(videoAddEvent, "https://youtube.com/tamere");
		    socket.off(videoAddEvent + success).on(videoAddEvent + success, function(video){
			socket.emit(videoDeleteEvent, video._id);
			socket.off(videoDeleteEvent + success).on(videoDeleteEvent + success, function(value){
			    assert(video._id, value.id);
			    resolve();
			});
			socket.off(videoDeleteEvent + err).on(videoDeleteEvent + err, function(err){
			    reject();
			});
		    });
		});
	    });
	});
	it('Not Existing', function() {
	    return new Promise(function(resolve, reject) {
		socket.emit(roomLoginEvent, roomToken);
		socket.off(roomLoginEvent + success).on(roomLoginEvent + success, function(data){
		    socket.emit(videoDeleteEvent, "test");
		    socket.off(videoDeleteEvent + success).on(videoDeleteEvent + success, function(value){
			console.log(value);
			reject();
		    });
		    socket.off(videoDeleteEvent + err).on(videoDeleteEvent + err, function(err){
			assert(err.id, "test");
			assert(err.msg, "VIDEO_NOT_EXISTING");
			resolve();
		    });
		});
	    });
	});
    });

    /*describe('Video moveUp', function() {
	it('True', function() {
	    return new Promise(function(resolve, reject) {
		socket.emit(roomLoginEvent, roomToken);
		socket.off(roomLoginEvent + success).on(roomLoginEvent + success, function(data){
		    socket.emit(videoAddEvent, "https://youtube.com/tamere");
		    socket.emit(videoAddEvent, "https://youtube.com/tamere1");
                    socket.off(videoAddEvent + success).on(videoAddEvent + success, function(video){
			assert(video.url, "https://youtube.com/tamere1");
			if (video.url === "https://youtube.com/tamere1"){
			    socket.emit(videoMoveUpEvent, video._id);
			    socket.off(videoMoveUpEvent + success).on(videoMoveUpEvent + success, function(id){
				assert(id, video._id);
				resolve();
			    });
			    socket.off(videoMoveUpEvent + err).on(videoMoveUpEvent + err, function(err){
				console.log(err);
				reject();
			    });
			}
		    });
		});
	    });
	});
    });*/
    
    /*describe('Video moveDown', function() {
	it('True', function() {
	    return new Promise(function(resolve, reject) {
		socket.emit(roomLoginEvent, roomToken);
		socket.off(roomLoginEvent + success).on(roomLoginEvent + success, function(data){
		    socket.emit(videoAddEvent, "https://youtube.com/tamere");
		    socket.emit(videoAddEvent, "https://youtube.com/tamere1");
                    socket.off(videoAddEvent + success).on(videoAddEvent + success, function(video){
			console.log(video._id);
			if (video.url === "https://youtube.com/tamere"){
			    socket.emit(videoMoveDownEvent, video._id);
			    socket.off(videoMoveDownEvent + success).on(videoMoveDownEvent + success, function(id){
				assert(id, video._id);
				resolve();
			    });
			    socket.off(videoMoveDownEvent + err).on(videoMoveDownEvent + err, function(err){
				console.log(err);
				reject();
			    });
			}
		    });
		});
	    });
	});
    });*/
    
    describe('Disconnect Testing', function() {
	before(function(done){
	    request(app)
                .post('/user/register')
                .send({username: username, password: user01})
                .expect(function(res){
                    userID = res.body.id;
                    //console.log(res.body);                                                                                                                                                                   
                })
                .end(function(err, res){
		    if (err) return resolve(err);
                    request(app)
                        .post('/user/login')
                        .send({username: username, password: user01})
                        .expect(function(res){
                            token = res.body.token;
                        })
                        .end(function(err, res){
			    //if (err) return reject(err);
			    request(app)
                                .post('/room/login')
                                .set("token", token)
                                .send({id: roomID})
                                .expect(function(res){
                                    roomToken2 = res.body.token;
                                })
                                .end(function(err, res){
                                    if (err) return reject(err);
                                    socket2 = io('http://127.0.0.1:1337/room-' + roomID, ioOptions);
                                    done();
                                })
                        })
                })
        });
    });
});
