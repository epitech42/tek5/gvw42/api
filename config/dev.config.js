module.exports = {
    apps : [{
	name        : "dev",
	script      : "src/index.js",
	watch       : true,
	ignore_watch : ["node_modules", "data", ".git", "Dockerfile", "DockerfileCI", "DockerfileTest", "docker-compose.yml", ".env", ".env.dist", "config/*", "#*#", "*~", "test/*"],
	watch_options: {
	    "followSymlinks": false
	},
	//exec_mode: "cluster",
    }]
}
